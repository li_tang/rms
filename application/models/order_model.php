<?php
/**
 * @author LIU XINLU
 * This model is to do create, read, update, delete function 
 * link to mysql database
 */
Class Order_Model extends CI_Model
{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	// insert function
	function Order_insert($arr)
	{
		$this->db->insert("order",$arr);
		return $this->db->insert_id();
	}

	public function get_bookingByStudentID($studentID)
	{
		$query = $this->db->get_where('order', array('studentID' => $studentID));               
		return $query->result_array();
	}
	
	public function get_AllBookings()
	{
		$query = $this->db->get('order');
		return $query->result_array();
	}
	
	public function get_bookingByOrderID($orderID)
	{
		$query = $this->db->get_where('order', array('orderID' => $orderID));               
		return $query->row_array();
	}
	public function set_bookingByOrderID($orderID,$data)
	{
		$this->db->where('orderID',$orderID);
		$this->db->update('order',$data);
		return 1;
	}
	
	function delete_Order($orderID) 
	{
		return $this->db->delete('order',array('orderID'=>$orderID));
	}
	
	function search_order($startDate,$studentID,$orderStatus)
	{
		$searchArray = array();
		if($startDate!='')
		{
			$searchArray['startDate']=$startDate;
		}
		if($studentID!='')
		{
			$searchArray['studentID']=$studentID;
		}
		if($orderStatus!='All')
		{
			$searchArray['orderStatus']=$orderStatus;
		}
		$query = $this->db->get_where('order', $searchArray);

		return $query->result_array();
	}
}

?>
