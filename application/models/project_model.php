<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Project_Model extends CI_Model {

	var $projectId ='';
	var $projectName ='';
	var $description ='';
	var $device ='';
	var $manufacture ='';
	var $image = '';
	
	private $table_comment = 'comment_for_project';

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database('default', 'db');
	}
	
	/**
	 * @author Andrew
	 * This function will insert the data into the database
	 */
	function insert_into_db()
	{
		$image = $_FILES['uploadedfile'];
		
		if (strlen($image['name'][0]) > 0) {
	
			//obtain data from form and insert into database
			if (isset($image) && array_shift($image['error'])!=4) {
			
				// Store file attributes, array_shift returns single first element of array
				$filename = array_shift($image['name']);
				$tmp_name = array_shift($image['tmp_name']);
				$mimetype = array_shift($image['type']);
				$filesize = array_shift($image['size']);
			}
			
			// If the server has magic quotes turned off, add slashes manually
			if (!get_magic_quotes_gpc()) {
				$filename = addslashes($filename);
			}
			
			// Open the file and extract the data/content from it
			$extract = fopen($tmp_name, 'r');
			$content = fread($extract, $filesize);
			$content = addslashes($content);
			fclose($extract);
		
			$title = $_POST['projectName'];
			$desc = $_POST['contentarea'];
			$coursecode = $_POST['coursecode'];
			$studentID = $_POST['studentID'];
			
			$this->db->query ("INSERT INTO project (title, description, coursecode, filename, filetype, filesize, filecontent, studentID, likeNum) VALUES
					('$title', '$desc', '$coursecode', '$filename', '$mimetype', '$filesize', '$content', '$studentID', 0)");
			return $this->db->insert_id();
		}
		else{
			
			$title = $_POST['projectName'];
			$desc = $_POST['contentarea'];
			$coursecode = $_POST['coursecode'];
			$studentID = $_POST['studentID'];
			 
			$this->db->query ("INSERT INTO project (title, description, coursecode, studentID, likeNum) VALUES
					('$title', '$desc', '$coursecode', '$studentID', 0)");
			return $this->db->insert_id();
			
		} 
	}
	
	
	/**
	 * @param  $arg1
	 * @param  $arg2
	 */
	function get_all_projects($arg1,$arg2)
	{
		$this->db->order_by('likeNum', 'desc');
		return $this->db->get('project',$arg1,$arg2)->result();
	}
	
	function get_project_by_id($crArray)
	{
		return $this->db->get_where('project',$crArray)->result();
	}
	
	/**
	 * @author yanbo.zhao
	 * function get all comment of 1 project according to projectID
	 */
	public function get_comments($projectID)
	{
		if(isset($projectID))
			$this->db->where(array('projectID' => $projectID));
		else
			return false;
	
		$this->db->select('*');
		//display comments by vote desc & time desc
		$this->db->order_by('likes', 'desc');
		$this->db->order_by('created_time', 'desc');
		$data = $this->db->get($this->table_comment)->result_array();
		return $data;
	}
	
	/**
	 * @author yanbo.zhao
	 * function to insert data into commentforproject table.
	 */
	public function insert($table, $data)
	{
		if(empty($data) || !isset($table))
			return false;
	
		$query = $this->db->insert($table,$data);
	
		if($query) {
			//get the new id have just insert to project table
			return $this->db->insert_id();
		}
		else {
			return false;
		}
	}
	
	/**
	 * @author yanbo.zhao
	 * function to get number of likes
	 */
	public function get_likes()
	{
		$query = $this->db->query('SELECT `comment_for_project`.`commentID`,`comment_for_project`.`likes`
        FROM comment_for_project');
		return $query = $query -> result_array();
	}
	
	/**
	 * @author yanbo.zhao
	 * function to increase to likes count
	 */
	public function add_likes($commentID,$likes)
	{
		$query = $this->db->get('comment_for_project');
		$query = $this->db->where('commentID',$commentID);
		$this->db->update('comment_for_project',array('likes' => $likes));
	}
	
	/**
	 * @author yanbo.zhao
	 * function to post increased likes number without refreshing the page
	 */
	public function java_function()
	{
		$like_function = "
			var commentID = $(this).prev().text();
		    var num_likes = parseInt ($(this).next().text()) + 1;
			$(this).next().text(num_likes);
			$.ajax({
				url: window.location,
				type: 'POST',
				data: {
					commentID: commentID,
					likes: num_likes,
				},
				success : function(msg){
			
				}
				});
		";
		$this->javascript->click('.comment-vote',$like_function);
		$this->javascript->compile();
	}
	
	/**
	 * Searching project in the database based on the search string 
	 * @param $searchStr string from the text input
	 * @param $searchOpt search option from radio button selected
	 * @param $pageSize size of the page
	 * @param $startingPageIndex the strating page index
	 */
	function search_project($searchStr,$searchOpt,$pageSize,$startingPageIndex)
	{
		$query="";
		$crossTableSearch = true;
		$tableDesc = $this->db->query("desc rms.project")->result();
		foreach($tableDesc as $row)
		{
			if($row->Field == $searchOpt)
			{
				$crossTableSearch = false;
			}
		}
	
		if($crossTableSearch)
		{
			$query = "SELECT p.projectID, p.title, p.coursecode, p.description, p.filename, p.filetype, p.filesize, p.filecontent, p.likeNum FROM rms.project e INNER JOIN rms.eqpt_usage_history u ON p.projectID = u.projectID INNER JOIN rms.equipment e on u.equipmentID = e.equipmentID WHERE p.$searchOpt like '%$searchStr%' LIMIT $startingPageIndex, $pageSize";
				
			//$query = "SELECT e.equipmentID, e.equipmentName, e.filetype, e.filecontent, e.quantity, e.loanStatus, e.loanPeriod, e.description, e.location, e.location, e.manufacturer, e.staffID FROM rms.equipment e INNER JOIN rms.eqpt_usage_history u ON e.equipmentID = u.equipmentID INNER JOIN rms.project p on u.projectID = p.projectID WHERE p.$searchOpt like '%$searchStr%' LIMIT $startingPageIndex, $pageSize";
		}
		else{
			$query = "select * from rms.project where $searchOpt like '%$searchStr%' LIMIT $startingPageIndex, $pageSize";
		}
		return $this->db->query($query)->result();
	}
	
	/**
	 * count number of projects after search prepare for pagination 
	 * @param  $searchStr
	 * @param  $searchOpt
	 */
	function count_search_project($searchStr,$searchOpt)
	{
		$query="";
		$crossTableSearch = true;
		$tableDesc = $this->db->query("desc rms.project")->result();
		foreach($tableDesc as $row)
		{
			if($row->Field == $searchOpt)
			{
				$crossTableSearch = false;
			}
		}
	
		if($crossTableSearch)
		{
			$query = "SELECT p.projectID, p.title, p.coursecode, p.description, p.filename, p.filetype, p.filesize, p.filecontent, p.likeNum FROM rms.project e INNER JOIN rms.eqpt_usage_history u ON p.projectID = u.projectID INNER JOIN rms.equipment e on u.equipmentID = e.equipmentID WHERE p.$searchOpt like '%$searchStr%'";
	
			//$query = "SELECT e.equipmentID, e.equipmentName, e.filetype, e.filecontent, e.quantity, e.loanStatus, e.loanPeriod, e.description, e.location, e.location, e.manufacturer, e.staffID FROM rms.equipment e INNER JOIN rms.eqpt_usage_history u ON e.equipmentID = u.equipmentID INNER JOIN rms.project p on u.projectID = p.projectID WHERE p.$searchOpt like '%$searchStr%' LIMIT $startingPageIndex, $pageSize";
		}
		else{
			$query = "select * from rms.project where $searchOpt like '%$searchStr%'";
		}
		return $this->db->query($query)->num_rows();
	}
	
	/**
	 * add likes to a specific project
	 * @param unknown $id project ID
	 */
	public function like_project($id){
		$this->db->where('projectID', $id);
		$this->db->set('likeNum', 'likeNum+1', FALSE);
		$this->db->update('project');
	}
	
	/**
	 * create a link between equipment and project
	 * @param unknown $equipmentList
	 * @param unknown $projectID
	 */
	public function insert_equipment_project_relation($equipmentList,$projectID)
	{
		foreach($equipmentList as $equipmentID)
		{
			$this->db->insert('eqpt_usage_history',
                                array('projectID' => $projectID,
                                      'equipmentID' => $equipmentID)
                            );
		}
		

	}
	
	/**
	 * update the relationship between equipment and project
	 * @param unknown $equipmentList
	 * @param unknown $projectID
	 */
	public function update_equipment_project_relation($equipmentList,$projectID)
	{
		//delete all the relation for this project first
		$this->db->delete('eqpt_usage_history', array('projectID' => $projectID)); 
		//insert new relation
		foreach($equipmentList as $equipmentID)
		{
			$this->db->insert('eqpt_usage_history',
					array('projectID' => $projectID,
							'equipmentID' => $equipmentID)
			);
		}
		
	}
	
	/**
	 * Get all the equipments used in a project
	 * @param $id the project ID
	 * @return a list of equipment ids that is used within a project
	 */
	public function get_equipment_project($id){
		/*$this->db->where('projectID', $id);
		return $this->db->get('eqpt_usage_history')->result();*/
		
		return $this->db->query("select * from rms.equipment where equipmentID in (select equipmentID from rms.eqpt_usage_history where projectID in (select projectID from rms.eqpt_usage_history where projectID = $id))")->result();
	}
	
	
	function select_project_by_userID(){
		$userdata = $this->session->userdata('UserID');
		$query = $this->db->query("select * from rms.project where studentID = '$userdata'");
		return $query->result();
		
	} 
	
	function select_project_by_projectID($id){
		$query = $this->db->query("select * from rms.project where projectID = $id");
		
		return $query->result();
	}
	
	function update_into_db()
	{
	
		$image = $_FILES['uploadedfile'];

		if (strlen($image['name'][0]) > 0) {
				  
			if (isset($image) && array_shift($image['error'])!=4) {
			
				// Store file attributes, array_shift returns single first element of array
				$filename = array_shift($image['name']);
				$tmp_name = array_shift($image['tmp_name']);
				$mimetype = array_shift($image['type']);
				$filesize = array_shift($image['size']);
			}
			
			// If the server has magic quotes turned off, add slashes manually
			if (!get_magic_quotes_gpc()) {
				$filename = addslashes($filename);
			}
			
			// Open the file and extract the data/content from it
			$extract = fopen($tmp_name, 'r');
			$content = fread($extract, $filesize);
					if (!get_magic_quotes_gpc()) {
			  $content = addslashes($content);
					}
			fclose($extract);
		
			$title = $_POST['projectName'];
			$desc = $_POST['contentarea'];
			$coursecode = $_POST['coursecode'];
			$studentID = $_POST['studentID'];
			$projectID = $_POST['projectID'];
			
			$this->db->query ("UPDATE project set title = '$title', filename = '$filename', filetype = '$mimetype', filesize = '$filesize', filecontent = '$content', description = '$desc', coursecode = '$coursecode', studentID = '$studentID' WHERE projectID = $projectID");
				
		}else {
			$title = $_POST['projectName'];
			$desc = $_POST['contentarea'];
			$coursecode = $_POST['coursecode'];
			$studentID = $_POST['studentID'];
			$projectID = $_POST['projectID'];
			$this->db->query ("UPDATE project set title = '$title', description = '$desc', coursecode = '$coursecode', studentID = '$studentID' WHERE projectID = $projectID");
				
		}
		 
		 
		return $this->db->insert_id();
	}
	
	
}