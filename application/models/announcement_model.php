<?php 

/**
 * @author WANG QI
 * This model is to retrive,insert, update and delete announcements from the database
 * link to mysql database
 */
class Announcement_Model extends CI_Model{

    public function __construct()
    {
             // Call the Model constructor
                parent::__construct(); 
                //loads the database
		$this->load->database();	
    }
    /**
    * @author WANG QI
    * This function retrieve the data from rms database
    */
    public function get_announcement($annID=FALSE)
	{
		//load html helper from codeIgniter
		$this->load->helper('html');
		if ($annID === FALSE)
		{
			//perform a query to get all announcement records ordered by the data in a descading sequence
                        $this->db->select('*');
                        $this->db->order_by('date','desc');
			$query = $this->db->get('announcement');
			return $query->result_array();
		}
	        
		//perform a query to get a announcement item by its annID
		$query = $this->db->get_where('announcement', array('annID' => $annID));               
		return $query->row_array();
	}
	
	public function set_announcement()
	{
		//load url helper from codeIgniter
		$this->load->helper('url');
                $this->load->helper('date');
                
		$title = url_title($this->input->post('title'), 'dash', TRUE);

		//assign an array with announcement infomation to data
		$data = array(
				'title' => $this->input->post('title'),
                                 'date' => $this->input->post('date'),
				'content' => $this->input->post('contentarea'),
				//automatically generate staffID using session
				'staffID' =>  $this->session->userdata('UserID'),
				
		);
	
		//insert data into announcement table in the database
		return $this->db->insert('announcement', $data);
	}
	
	function delete_announcement($annID) 
	{	
		return $this->db->delete('announcement',array('annID'=>$annID));
	}
        
	function update_announcement()
	{
		//load url helper from codeIgniter
		$this->load->helper('url');

		$annID= url_title($this->input->post('annID'), 'dash', TRUE);
		
		//assign an array with announcement infomation to data
		$data = array(
								'annID'=>  $this->input->post('annID'),
				'title' => $this->input->post('title'),
								 'date' => $this->input->post('date'),
				'content' => $this->input->post('contentarea'),
				//automatically generate staffID using session
				'staffID' =>  $this->session->userdata('UserID'),
				
		);
	    return $this->db->update('announcement', $data, array('annID' => $annID));
	}
}
?>