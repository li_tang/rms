<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Equipment_Model extends CI_Model {

	var $equipmentId ='';
	var $name ='';
	var $image ='';
	var $quantity ='';
	var $loanStatus ='';
	var $description ='';
	var $location ='';
	var $manufacture ='';
	var $staffID = '';

	private $table_comment = 'comment_for_equipment';
	
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database('default', 'db');
	}

	/**
	 * @author li.tang
	 * This function search result based on the search string and search option passed in
	 * @param $searchStr: the keywork needed to be searched 
	 * @param $searchOpt: the option needed to be searched, the search option need to be the same as database
	 * column name
	 * @param $pageSize: how many item u want to display per search option
	 * @param $startingPageIndex: from which index position of your search result you want to return 
	 */
	function search_equipment($searchStr,$searchOpt,$pageSize,$startingPageIndex)
	{
		$query="";
		$crossTableSearch = true;
		$tableDesc = $this->db->query("desc rms.equipment")->result();
		foreach($tableDesc as $row)
		{
			if($row->Field == $searchOpt)
			{
				$crossTableSearch = false;
			}
		}
		
		if($crossTableSearch)
		{
			$query = "SELECT e.equipmentID, e.equipmentName, e.filetype, e.filecontent, e.quantity, e.loanStatus, e.loanPeriod, e.description, e.location, e.location, e.manufacturer, e.staffID FROM rms.equipment e INNER JOIN rms.eqpt_usage_history u ON e.equipmentID = u.equipmentID INNER JOIN rms.project p on u.projectID = p.projectID WHERE p.$searchOpt like '%$searchStr%' LIMIT $startingPageIndex, $pageSize";
		}
		else{
			$query = "select * from rms.equipment where $searchOpt like '%$searchStr%' LIMIT $startingPageIndex, $pageSize";
		}
		return $this->db->query($query)->result();
	}
	
	/**
	 * count number equipment to prepare search pagination
	 * @param unknown $searchStr
	 * @param unknown $searchOpt
	 */
	function count_search_equipment($searchStr,$searchOpt)
	{
		$query="";
		$crossTableSearch = true;
		$tableDesc = $this->db->query("desc rms.equipment")->result();
		foreach($tableDesc as $row)
		{
			if($row->Field == $searchOpt)
			{
				$crossTableSearch = false;
			}
		}
	
		if($crossTableSearch)
		{
			$query = "SELECT e.equipmentID, e.equipmentName, e.filetype, e.filecontent, e.quantity, e.loanStatus, e.loanPeriod, e.description, e.location, e.location, e.manufacturer, e.staffID FROM rms.equipment e INNER JOIN rms.eqpt_usage_history u ON e.equipmentID = u.equipmentID INNER JOIN rms.project p on u.projectID = p.projectID WHERE p.$searchOpt like '%$searchStr%'";
		}
		else{
			$query = "select * from rms.equipment where $searchOpt like '%$searchStr%'";
		}
		return $this->db->query($query)->num_rows();
	}
	
	/**
	 * @author li.tang
	 * This function retrieve all the records for equipment
	 */
	function get_all_equipment($arg1,$arg2)
	{
		return $this->db->get('equipment',$arg1,$arg2)->result();
	}
	
	/**
	 * @author li.tang
	 * This function will query equipment by equipment criteria
	 */
	function get_equipment_by_criteria($crArray)
	{
		return $this->db->get_where('equipment',$crArray)->result();
	}
	/**
	 * @author Andrew
	 * This function will delete equipment by equipment criteria
	 */
	function delete_equipment_by_id($id) 
    {
    	$crArray = array('equipmentID' => $id);
    	$this->db->delete('comment_for_equipment',$crArray);
		return $this->db->delete('equipment',$crArray);
	}
	
	/**
	 * check a specific equipment is ordered 
	 * @param  $id equipment ID
	 * @return boolean
	 */
	function equipment_is_ordered($id)
	{
		$crArray = array('equipmentID' => $id,'orderStatus' => 'Pending');
		$query = $this->db->get_where('order',$crArray);
		if($query->num_rows()>0)
		{
			return true;
		}
		else {
			return false;
		}
	}
	/**
	 * @author Andrew
	 * This function will insert the data into the database
	 */
	function insert_into_db()
	{
		//obtain data from form and insert into database
		$image = $_FILES['uploadedfile'];
		
		if (strlen($image['name'][0]) > 0) {
		
			if (isset($image) && array_shift($image['error'])!=4) {
			
				// Store file attributes, array_shift returns single first element of array
				$filename = array_shift($image['name']);
				$tmp_name = array_shift($image['tmp_name']);
				$mimetype = array_shift($image['type']);
				$filesize = array_shift($image['size']);
			}
			
			// If the server has magic quotes turned off, add slashes manually
			if (!get_magic_quotes_gpc()) {
				$filename = addslashes($filename);
			}
			
			// Open the file and extract the data/content from it
			$extract = fopen($tmp_name, 'r');
			$content = fread($extract, $filesize);
			$content = addslashes($content);
			fclose($extract);
		
			$equipmentName = $_POST['equipmentName'];
			$quantity = $_POST['quantity'];
			$loanStatus = $_POST['loanStatus'];
			$description = $_POST['description'];
			$location = $_POST['location'];
			$manufacturer = $_POST['manufacturer'];
			$staffID = $_POST['staffID'];
			$loanPeriod = $_POST['loanPeriod'];
			$this->db->query ("INSERT INTO equipment (equipmentName, filename, filetype, filesize, filecontent, quantity, loanStatus, description, location, manufacturer, staffID, loanPeriod) VALUES
				('$equipmentName', '$filename', '$mimetype', '$filesize', '$content', '$quantity', '$loanStatus', '$description', '$location', '$manufacturer', '$staffID', '$loanPeriod')");
		}
		else {
			
			$equipmentName = $_POST['equipmentName'];
			$quantity = $_POST['quantity'];
			$loanStatus = $_POST['loanStatus'];
			$description = $_POST['description'];
			$location = $_POST['location'];
			$manufacturer = $_POST['manufacturer'];
			$staffID = $_POST['staffID'];
			$loanPeriod = $_POST['loanPeriod'];
			$this->db->query ("INSERT INTO equipment (equipmentName, quantity, loanStatus, description, location, manufacturer, staffID, loanPeriod) VALUES
					('$equipmentName', '$quantity', '$loanStatus', '$description', '$location', '$manufacturer', '$staffID', '$loanPeriod')");
		
		}
	}
	
	/**
	 * modify equipment
	 */
	function edit_into_db()
	{
		$image = $_FILES['uploadedfile'];

		if (strlen($image['name'][0]) > 0) {
				  
			if (isset($image) && array_shift($image['error'])!=4) {
			
				// Store file attributes, array_shift returns single first element of array
				$filename = array_shift($image['name']);
				$tmp_name = array_shift($image['tmp_name']);
				$mimetype = array_shift($image['type']);
				$filesize = array_shift($image['size']);
			}
			
			// If the server has magic quotes turned off, add slashes manually
			if (!get_magic_quotes_gpc()) {
				$filename = addslashes($filename);
			}
			
			// Open the file and extract the data/content from it
			$extract = fopen($tmp_name, 'r');
			$content = fread($extract, $filesize);
			if (!get_magic_quotes_gpc()) {
				$content = addslashes($content);
			}
			fclose($extract);
				   
			$equipmentID = $_POST['equipmentID'];
			$equipmentName = $_POST['equipmentName'];
			$quantity = $_POST['quantity'];
			$loanStatus = $_POST['loanStatus'];
			$description = $_POST['description'];
			$location = $_POST['location'];
			$manufacturer = $_POST['manufacturer'];
			$staffID = $_POST['staffID'];
			$loanPeriod = $_POST['loanPeriod'];
			
			$this->db->query ("UPDATE equipment set equipmentName = '$equipmentName', filename = '$filename', filetype = '$mimetype', filesize = '$filesize', filecontent = '$content', quantity = '$quantity', loanStatus ='$loanStatus', description = '$description', location = '$location', manufacturer = '$manufacturer', staffID = '$staffID', loanPeriod = '$loanPeriod' WHERE equipmentID = $equipmentID");
		}
		else {
			
			$equipmentID = $_POST['equipmentID'];
			$equipmentName = $_POST['equipmentName'];
			$quantity = $_POST['quantity'];
			$loanStatus = $_POST['loanStatus'];
			$description = $_POST['description'];
			$location = $_POST['location'];
			$manufacturer = $_POST['manufacturer'];
			$staffID = $_POST['staffID'];
			$loanPeriod = $_POST['loanPeriod'];
			
			$this->db->query ("UPDATE equipment set equipmentName = '$equipmentName', quantity = '$quantity', loanStatus ='$loanStatus', description = '$description', location = '$location', manufacturer = '$manufacturer', staffID = '$staffID', loanPeriod = '$loanPeriod' WHERE equipmentID = $equipmentID");
		}	
	}
	
	/**
	 * @author yanbo.zhao
	 * function get all comment of 1 equipment according to equipmentID
	 */
	public function get_comments($equipmentID)
	{
		if(isset($equipmentID))
			$this->db->where(array('equipmentID' => $equipmentID));
		else
			return false;
	
		$this->db->select('*');
		//display comments by vote desc & time desc
		$this->db->order_by('likes', 'desc');
		$this->db->order_by('created_time', 'desc');
		$data = $this->db->get($this->table_comment)->result_array();
		return $data;
	}
	
	/**
	 * @author yanbo.zhao
	 * function to insert data into commentforequipment table.
	 */
	public function insert($table, $data)
	{
		if(empty($data) || !isset($table))
			return false;
	
		$query = $this->db->insert($table,$data);
	
		if($query) {
			//get the new id have just insert to equipment table
			return $this->db->insert_id();
		}
		else {
			return false;
		}
	}
	
	/**
	 * @author yanbo.zhao
	 * function to get number of likes
	 */
	public function get_likes()
	{
		$query = $this->db->query('SELECT `comment_for_equipment`.`commentID`,`comment_for_equipment`.`likes`
        FROM comment_for_equipment');
		return $query = $query -> result_array();
	}
	
	/**
	 * @author yanbo.zhao
	 * function to increase to likes count
	 */
	public function add_likes($commentID,$likes)
	{
		$query = $this->db->get('comment_for_equipment');
		$query = $this->db->where('commentID',$commentID);
		$this->db->update('comment_for_equipment',array('likes' => $likes));
	}
	
	/**
	 * @author yanbo.zhao
	 * function to post increased likes number without refreshing the page
	 */
	public function java_function()
	{
		$like_function = "
			var commentID = $(this).prev().text();
		    var num_likes = parseInt ($(this).next().text()) + 1;
			$(this).next().text(num_likes);
			$.ajax({
				url: window.location,
				type: 'POST',
				data: {
					commentID: commentID,
					likes: num_likes,
				}, 
				success : function(msg){
					
				}
				});	
		";
		$this->javascript->click('.comment-vote',$like_function);
		$this->javascript->compile();
	}

	/**
	 * Retrieve all records of equipments for search box
	 */
	function get_equipment_list(){
		$this->db->select('equipmentName, equipmentID' );
		return $this->db->get('equipment')->result();
	}
	
    /**
	 * @author LIU XINLU
	 * this function gets loanperiod based on equipmentID
	 */
	public function get_loanPeriod($equipmentID)
	{   
		$this->db->select("loanPeriod");
		$this->db->from('equipment');   
		$this->db->where('equipmentID', $equipmentID);
		return $this->db->get()->result();
          
	}
        
     /**
	 * @author LIU XINLU
	 * this function updates the lastest quantity based on equipmentID
	 */
	public function update_quantity($quantity,$equipmentID)
	{
		$this->db->set('quantity', $quantity, FALSE);
		if($quantity == 0)
		{
			$this->db->set('loanStatus', 'N', true);
		}
		else if ($quantity >0)
		{
			$this->db->set('loanStatus', 'Y', true);
		}
		$this->db->where('equipmentID', $equipmentID);
		$this->db->update('equipment');  
	}
        
	/**
	 * get equipment by ID
	 * @param unknown $equipmentID
	 */
	public function get_equipment_info($equipmentID)
	{   
		$this->db->select("*");
		$this->db->from('equipment');   
		$this->db->where('equipmentID', $equipmentID);
		return $this->db->get()->result();
	}
	
	/**
	 * get a list of recommended equipment
	 * @param $equipmentID
	 */
	public function get_recommended_equipment($equipmentID)
	{
		return $this->db->query("select * from rms.equipment 
				where equipmentID in (select equipmentID from rms.eqpt_usage_history 
				where projectID in (select projectID from rms.eqpt_usage_history 
				where equipmentID = $equipmentID)) ORDER BY RAND() limit 6")->result();
	}
}