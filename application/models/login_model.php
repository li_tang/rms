<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author WANG QI
 * 
 */
class Login_Model extends CI_Model{

    public function __construct()
    {
        // Call the Model constructor
           parent::__construct();     
    }
    /**
    * @author WANG QI
    * This function retrieve the data from userdatabase
    */
    public function user_select($UserID)
    {
            // Load the userdb to retrieve the user infromation
            // Use the user input($UserID) as the select key and then return the data
            $userdb = $this->load->database('userA',true);
            $userdb->where('UserID',$UserID);
            $userdb->select('*');
            $query=  $userdb->get('user');
            return $query->result();
    }
}
?>
