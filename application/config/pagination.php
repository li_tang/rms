<?php
$config['use_page_numbers'] = true;
$config['first_link'] = 'First page';
$config['last_link'] = 'Last';
$config['next_link'] = '&gt;';
$config['prev_link'] = '&lt;';
$config['cur_tag_open'] = '<b>';
$config['cur_tag_close'] = '</b>';