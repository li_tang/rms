<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <?php if($this->session->userdata('Category')!='Staff'){     redirect('login');}?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Announcement</title>
    </head>
    <body>
    <div id="wrapper">
     	<div id="content">
			<div id="content-inner">
				<div class="content-center">
					<div class="content-header"><h3>Announcement</h3></div>
					<br>


	 				<div><?php echo anchor('announcement/createAnnouncement','Create Announcement');?></div>
	 				<table class='regular'>
	    				 <thead><th>Title</th><th>Date</th><th>View</th><th>Update</th><th>Delete</th></thead>
	        				<?php foreach( $announcement as $announcement_item): ?>  
	       					 <tr>
	      						 <td><?php echo $announcement_item['title'] ?></td>
	      						 <td><?php echo $announcement_item['date']?></td>
	    						 <td><?php echo anchor('announcement/'.$announcement_item['annID'],'View');?></td>
	  							 <td><?php echo anchor('announcement/updateAnnouncement/'.$announcement_item['annID'],'Update'); ?></td>
	   							 <td><?php echo anchor('announcement/deleteAnnouncement/'.$announcement_item['annID'],'Delete'); ?></td>
	    					 </tr>
							<?php endforeach ?>
					</table>
				</div>
			</div>
		</div>
	</div>

    </body>
</html>
