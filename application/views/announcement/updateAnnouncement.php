<html DOCTYPE!>
      <?php if($this->session->userdata('Category')!='Staff'){   redirect('login');}?>
<head>
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<title>Update Announcement</title>
</head>
<body>

	<div id="wrapper">
		<div id="content">
			<div id="content-inner">
				<div class="content-center">
					<div class="content-header"><h3>Update Announcement</h3></div><br>


					<!-- check form validation and open form for creating announcement -->
					<?php echo validation_errors(); ?>
					<?php echo form_open() ?>
					<form action=<?php echo site_url('announcement/updateAnnouncement');?> method="POST">
						<!-- required field for staff to type in announcement title --> 	    
					    <p><label for="Title">Title</label>
					       <input type="text" name="title" value="<?php echo $announcement_item['title'];?>" required/>
					    </p>

						<!-- required field for staff to enter content of the announcement -->
						<p><label for="content">Content</label>
					       <textarea type="input" name="contentarea"><?php echo $announcement_item['content'];?></textarea>
					    </p>
						<script type="text/javascript">
			                             CKEDITOR.replace( 'contentarea' );
			                    	</script>
						<!-- required field for staff to type in the date -->
						<p><label for="date">Date</label>
						   <input type="text" name="date" value="<?php echo $announcement_item['date']?>" readonly/>
						</p>
					       <input type='hidden'name='annID' value="<?php echo $announcement_item['annID']?>"/>
		
                                                <input type="submit" name="submitForm" value="Update" class="submitBelow" />
                                             
					
					</form>
				</div>
			</div>
		</div>

    </div>
</body>
</html>


