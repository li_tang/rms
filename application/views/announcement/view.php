<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <?php if($this->session->userdata('Category')!='Staff'){     redirect('login');}?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View an Announcement</title>
    </head>
    <body>
    <div id="wrapper">
    	<div id="content">
			<div id="content-inner">
				<div class="content-center">
					<div class="content-header"><h3>View Announcement</h3></div>

    						<table class="regular">
       							 <tr>
       							 <td>Title:</td>  
       							 <td><?php echo $announcement_item['title'];?></td>
       							 </tr>
  								 <tr>
  								 <td>Content:</td>
          						<td><?php echo $announcement_item['content'];?></td>
          						</tr>
          						<tr>
          						<td>Date:</td>
          						<td><?php echo $announcement_item['date'];?></td>
         						 </tr>
       						</table>
					        <div align='center'>
					            <a><?php echo anchor('announcement/updateAnnouncement/'.$announcement_item['annID'],'Update'); ?></a>
					            &nbsp; &nbsp;
                                                    <a> <?php echo anchor('announcement/deleteAnnouncement/'.$announcement_item['annID'],'Delete'); ?> </a> 
                                                    &nbsp;&nbsp;
                                                    <a> <?php echo anchor('announcement','Back');?></a>
					        </div>
					        <br>
				 </div>
			</div>
		</div>

    </div>
    </body>
</html>
