<html DOCTYPE!>
      <?php if($this->session->userdata('Category')!='Staff'){     redirect('login');}?>
<head>
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<title>Form</title>
       
</head>
<body>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/customise.css" />
	<div id="wrapper">
		<div id="content">
			<div id="content-inner">
				<div class="content-center">
					<div class="content-header"><h3>Create Announcement</h3></div>
					<br>
					
					<!-- check form validation and open form for creating announcement -->
					<?php echo validation_errors(); ?>
					<?php echo form_open('announcement/createAnnouncement') ?>
						<!-- required field for staff to type in announcement title --> 	    
						<p><label for="Title">Title</label>
						   <input type="input" name="title" required/>
						</p>
						<!-- required field for staff to enter content of the announcement -->
						<p><label for="contentarea">Content</label>
						   <textarea name="contentarea" required></textarea>
                                                   <script type="text/javascript">
			                             CKEDITOR.replace( 'contentarea' );
			                    	</script>
						</p>
						<!-- required field for staff to type in the date -->
	        			<input type="hidden" name="date" value="<?php echo date("Y-m-d");?>"/>
						<p>
                                                    <label></label>
	       				   <input type="submit" name="submitForm" value="Create" class="submitBelow" />
                                                
                                        

	        			</p>
					</form>
				</div>
			</div>
		</div>
	</div>

</body>
</html>


