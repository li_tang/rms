
<div id="content">
	<div id="content-inner">
		<!--loading of the search bar -->
		<?php $this->load->view('templates/searchBar');?>
		<?php if($this->session->userdata('Category')=="Student"){?>
		<div class="content-left">
			<div class="content-center">
				<div class="content-header"><h3>My Courses</h3></div>
				<ul class="link-list">
				<?php $course1 = $this->session->userdata('course1');
					  $course2 = $this->session->userdata('course2');
					  $course3 = $this->session->userdata('course3');
					  $course4 = $this->session->userdata('course4');
				?>
					<li><a href=<?php echo site_url("listEquipment/search?searchEquipment=".$course1."&searchOpt=course");?>><?php echo "[".$course1."]";?></a></li>
					<li><a href=<?php echo site_url("listEquipment/search?searchEquipment=".$course2."&searchOpt=course");?>><?php echo "[".$this->session->userdata('course2')."]";?></a></li>
					<li><a href=<?php echo site_url("listEquipment/search?searchEquipment=".$course3."&searchOpt=course");?>><?php echo "[".$this->session->userdata('course3')."]";?></a></li>
					<li><a href=<?php echo site_url("listEquipment/search?searchEquipment=".$course4."&searchOpt=course");?>><?php echo "[".$this->session->userdata('course4')."]";?></a></li>
				</ul>
			</div>
		</div>
		<?php }?>
		<!--listing for student -->
	    <?php if($this->session->userdata('Category')=="Student")
	    { ?>
			<div class="content-right">
				<div class="content-center">
					<div class="content-header"><h3>Equipment List</h3></div>
					
					<?php if($found){?>
						<ul class="result-list">
					<?php foreach($tableContent as $row){?>
						<li>
							<a href=<?php echo site_url("viewEquipment/index/".$row->equipmentID);?>>
								
								
								<?php if ($row->filecontent == null){?>
										<div class="result-img"><img src=<?php echo base_url()."images/noimage.gif"?> alt="Noimage"></div>
									<?php } else {?>
								<div class="result-img"><?php echo '<img src="data:'.$row->filetype.';base64,' . base64_encode( $row->filecontent ) . '" />';?></div>
								<?php }?>
								<div class="result-desc">									
									<h3><?php echo $row->equipmentName;?></h3>
									<p><?php echo $row->description;?></p>
									<p>Quantity: <?php echo $row->quantity;?>&nbsp;&nbsp;&nbsp;Location: <?php echo $row->location;?></p>
									<p><?php 
							 			if($row->loanStatus == 'Y')
							 			{
							 				echo '<font color="green">Available</font>';
							 			}
							 			else
							 			{
											echo '<font color="red">Not Available</font>';
							 			}
									?></p>
								</div>
							</a>
						</li>
					<?php }?>
					</ul>
					<?php }else{ echo "<font color='red'>No record found !</font>";
					}?>
						
				</div>				
			</div>
		<!--list for staff -->
		<?php 
		}
		else
		{
			echo "<div class='content-center'>";
			echo "<div class='content-header'><h3>Equipment List</h3></div><br>";
			echo anchor('addEquipment',"Add Equipment <br>");
			if($found){
				echo $this->table->generate($tableContent);
			}else{
				echo "<font color='red'>No record found !</font>";
			}
			echo "</div>";
		 }?>
		<?php echo $this->pagination->create_links();?>
	</div>
</div>
