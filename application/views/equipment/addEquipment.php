<!DOCTYPE html> 
<html>
<?php if($this->session->userdata('Category')!='Staff'){     redirect('login');}?>

<div id="content">
			<div id="content-inner">
				<div class="content-center">
						<div class="content-header"><h3>Add Equipment</h3></div>
						<br>

<form action=<?php echo site_url('addEquipment/insert_to_db');?> method="post" enctype="multipart/form-data">

						<p><label for="equipmentName">Equipment Name:</label>
							<input name="equipmentName" class="element text medium" type="text" maxlength="255" value="" required/> 
						</p>
						
						<p><label for="image">Image:</label>
							
                			<input type="hidden" name="image" />
                			Choose a file to upload: <input name="uploadedfile[]" type="file" accept="image/*" /><br />
               			 	 
						</p>
						
						<p><label for="quantity">Quantity:</label>
                			<select class="element select medium" id="element_7" name="quantity"> 
		                        <?php
								$range = range(0,100);
								foreach ($range as $cm) {
								  echo "<option value='$cm'>$cm </option>";
								}    
		                        ?>
	                    	</select>
						</p>
						
						
						
						
						<p><label for="location">Location:</label>
							<input name="location" class="element text medium" type="text" maxlength="255" value="" required/> 
						</p>
						
						<p><label for="manufacturer">Manufacturer:</label>
							<input name="manufacturer" class="element text medium" type="text" maxlength="255" value="" required/> 
						</p>
						
						<p><label for="description">Description:</label>
							<textarea name="description" class="element textarea medium" required></textarea>
						</p>
						
						<p><label for="loanPeriod">Loan Period:</label>
							
                			<select class="element select medium" name="loanPeriod"> 
		                        <?php
								$range = range(0,365);
								foreach ($range as $cm) {
								  echo "<option value='$cm'>$cm days</option>";
								}    
		                        ?>
	                    	</select>
						</p>
						
						
						<p><label for="loanStatus">Loan Status:</label>
							<span>
				                <input name="loanStatus" class="element radio" type="radio" value="Y" checked />
				                <label class="choice" for="element_8_1">Available</label>
				                <input name="loanStatus" class="element radio" type="radio" value="N" />
				                <label class="choice" for="element_8_2">Not Available</label>
				            </span>
						</p>
						
						<p><label for="staffID">Staff ID:</label>
							<input name="staffID" class="element text medium" type="text" readonly maxlength="255" value="<?PHP echo $this->session->userdata('UserID') ?>" disabled/>
						</p>
						
						<p><input class="submitBelow" type="submit" value="Create"></p>
					</form>

    		</div>
			</div>
		</div>
</body>
</html>

<!-- include js&css file for viewEquipment-->

