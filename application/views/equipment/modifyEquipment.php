<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->


<?php if($this->session->userdata('Category')!='Staff'){     redirect('login');}?>
<div id="content">
			<div id="content-inner">
				<div class="content-center">
					<div class="content-header"><h3>Edit Equipment</h3></div>
						<br>
					<?php echo validation_errors(); ?>
				<form action=<?php echo site_url('modifyEquipment/edit_to_db');?> method="post" enctype="multipart/form-data">
				
					<?php foreach($equipmentDetail as $row){?>


						<input type="hidden" name="equipmentID" value= "<?php echo $row->equipmentID;?>">	
						
						
						<p><label for="equipmentName">Equipment Name:</label>
							<input name="equipmentName" class="element text medium" type="text" maxlength="255" value="<?php echo $row->equipmentName;?>" required/> 
						</p>
											
						
						<p><label for="image">Image:</label>
							
                			<input type="hidden" name="image" />
                			Choose a file to upload: <input name="uploadedfile[]" type="file" accept="image/*" /><br />
               			 	
						</p>
						
					
						
						<p><label for="quantity">Quantity:</label>
							<select class="element select medium" id="element_7" name="quantity"> 
							
							<?php
							$range = range(0,1000);
							foreach ($range as $cm) {
					                        if ($cm == $row->quantity) {
					                            echo "<option value='$cm' selected='selected'>$cm</option>";
					                        } else { 
								    echo "<option value='$cm'>$cm</option>";
					                        }
							}    
					         ?>
					         </select>
							
                			
						</p>
						
						
						
						
						
						
						<p><label for="location">Location:</label>
							<input name="location" class="element text medium" type="text" maxlength="255" value="<?php echo $row->location;?>" required/> 
						</p>
						
						<p><label for="manufacturer">Manufacturer:</label>
							<input name="manufacturer" class="element text medium" type="text" maxlength="255" value="<?php echo $row->manufacturer;?>" required/> 
						</p>
						
						<p><label for="description">Description:</label>
							<textarea name="description" class="element textarea medium" required><?php echo $row->description;?></textarea>
						</p>
						
						<p><label for="loanPeriod">Loan Period:</label>
						<select class="element select medium" name="loanPeriod"> 
						 <?php
							$range = range(0,365);
							foreach ($range as $cm) {
					                        if ($cm == $row->loanPeriod) {
					                            echo "<option value='$cm' selected='selected'>$cm days</option>";
					                        } else { 
								    echo "<option value='$cm'>$cm days</option>";
					                        }
							}    
					         ?>
						</select>
						</p>
						
						<p><label for="staffID">Staff ID:</label>
							<input name="staffID" class="element text medium" type="text" readonly maxlength="255" value="<?PHP echo $this->session->userdata('UserID') ?>"/>
						</p>
						
						<p><label for="loanStatus">Loan Status:</label>
							<span>
				                <input name="loanStatus" class="element radio" type="radio" value="Y" />
				                <label class="choice" for="element_8_1">Available</label>
				                <input name="loanStatus" class="element radio" type="radio" value="N" />
				                <label class="choice" for="element_8_2">Not Available</label>
				            </span>
						</p>
						
						
						
						<p><input class="submitBelow" type="submit" value="Update"></p>
					

    					<?php }?>
    					
					</form>
				
		</div>
</div>
</div>
		


<!-- include js&css file for viewEquipment-->

<script src="<?php echo base_url().'assets/js/scriptforequipment.js'; ?>"></script> 