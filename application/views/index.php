<html DOCTYPE!>
<head>
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<title>Student Home Page</title>
</head>
<body>
	<div id="wrapper">
		<div id="content">
			<div id="content-inner">
			<?php $this->load->view('templates/searchBar');?>
				 <div class="content-center">
					<div class="content-header"><h3>Announcements</h3></div>
					<ul class="announcement-list">

                    <?php $i=1;?>
  					<?php foreach( $announcement as $announcement_item) : if(++$i > 7) break; ?>                      
	                    <li> </br><h3><?php echo $announcement_item['title'] ?> </h3>
	                   <p><?php echo $announcement_item['content']?><br/><br/><span class="date"><?php echo $announcement_item['date']?></span></p></li>                   
                    <?php endforeach ?>

                    </ul>
            	 </div>           
			 </div>
		 </div>
     </div>
</body>
</html>