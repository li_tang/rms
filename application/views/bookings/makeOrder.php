<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<?php $row = $equipment[0]; ?>
<script>
	$(function() {
	  $( "#datepicker" ).datepicker( 
                  { dateFormat: "yy-mm-dd" , minDate:'0'}
	  );
          
          var availableVal = '<?php echo $row->quantity;?>';
          //alert(parseInt(currentVal));
          $('#quantitySlider').slider(
             {
                range: "max",
                min: 1,
                max: parseInt(availableVal),
                value: 1,
                slide: function( event, ui ) {
                    $( "#qVal" ).html(ui.value);
                    $( "input#quantity" ).val( ui.value );
                }
             }
            );
	  });
</script>
<script>
    function validateForm()
{
var x=document.forms["orderForm"]["startDate"].value;
if (x==null || x=="")
  {
  alert("Please select a pickup date");
  return false;
  }
}
</script>
<?php if($this->session->userdata('Category')!='Student'){   redirect('login');}?>
<div id="content">
	<div id="content-inner">
		<div class="content-left content-center">
			<div class="content-header"><h3>Booking Details</h3></div>
			<ul class="link-list">
				<li>Equipment ID: 
                                    <?php
                                        echo $row->equipmentID;
                                        //var_dump($row);
                                     ?>
                                </li>
                                <li>Equipment Name: <?php echo $row->equipmentName;?></li>
                                <li>Quantity: <?php echo $row->quantity; ?></li>
				<li>Email:<?php echo $this->session->userdata('email');?></li>
				<li>Mobile No: <?php echo $this->session->userdata('mobileNo');?></li>
			</ul>
		</div>
					
		<div class="content-right content-center">
			<div class="content-header"><h3>Booking Form</h3></div>
			<ul class="result-list" >
				<form style="margin-left: 140px" name="orderForm" onsubmit="return validateForm()" method="POST" action=<?php echo site_url('Order/insert');?>  >
									
				<p><label for="reason">Quantity:</label>
					<input type="hidden" name="quantity" id="quantity" value="1" />
                                        <div id="quantitySlider"></div><br/><div id="qVal">1</div>
                                        <input type="hidden" name="eid" value="<?php echo $row->equipmentID;?>" />
                                        <input type="hidden" name="loanPeriod" value="<?php echo $row->loanPeriod; ?>" />
                                        <input type="hidden" name="totalquantity" value="<?php echo $row->quantity;?>" />
				</p>
							  
				<p><label for="pickup">Pick-up date:</label>
					<input type="text" id="datepicker"   name="startDate" />
				</p>
				<p><label>Period:</label><?php echo $row->loanPeriod;?> days</p>
				<p><input class="submitBelow" type="submit" value="Book" name="submit" ></p>
			</form>
			</ul>					
		</div>
	</div>
</div>
 	