<html DOCTYPE>
  <?php if($this->session->userdata('Category')!='Student'){   redirect('login');}?>
  
    <head>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<?php $row = $equipment[0]; ?>

<script type="text/javascript">

function ComfirmCancelOrder()
{
  var r=confirm("Are you sure you want to cancel this?");
  if(r == true){
    window.location.href="http://localhost/rmsL/orderManagement/index.php";  }else{
   return false;
  }
}
</script>


<script type="text/javascript">
       
	 $(function() {
	  $( "#datepicker" ).datepicker( 
                  { dateFormat: "yy-mm-dd" , minDate:'0'}
	  );
          $("#datepicker").val("<?php echo $order['startDate']; ?>");
          
          var currentVal = '<?php echo $order['quantity'];?>';
          var availableVal = '<?php echo (intval($order['quantity']) + intval($row->quantity))?>';
          //alert(parseInt(currentVal));
          $('#quantitySlider').slider(
             {
                range: "max",
                min: 1,
                max: parseInt(availableVal),
                value: parseInt(currentVal),
                slide: function( event, ui ) {
                    $( "#qVal" ).html(ui.value);
                    $( "input#quantity" ).val( ui.value );
                }
             }
            );
	  });
</script>
<script>
    function validateForm()
{
var x=document.forms["updateForm"]["startDate"].value;
if (x==null || x=="")
  {
  alert("Please select a pickup datet");
  return false;
  }
}
    </script>
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<title>Update Booking</title>
</head>
<body>
	<div id="wrapper">
		<div id="content">
			<div id="content-inner">
			<div class="content-left content-center">
			<div class="content-header"><h3>Booking Details</h3></div>
			<ul class="link-list">
				<li>Equipment ID: 
                                    <?php
                                        echo $row->equipmentID;
                                        //var_dump($row);
                                     ?>
                                </li>
                                <li>Equipment Name: <?php echo $row->equipmentName;?></li>
                                <li>Quantity: <?php echo $row->quantity; ?></li>
				<li>Email:<?php echo $this->session->userdata('email');?></li>
				<li>Mobile No: <?php echo $this->session->userdata('mobileNo');?></li>
			</ul>
		</div>
				<div class="content-center content-right">
					<div class="content-header"><h3>Update Booking</h3></div>
					<br>
					<!-- check form validation and open form for creating announcement -->
					<?php echo validation_errors(); ?>
					<form name="updateForm" onsubmit="return validateForm()"action=<?php echo site_url('orderManagement/UpdateOrder');?> method="POST">
						<!-- required field for staff to type in announcement title --> 	 
						
					    <p><label for="reason">Quantity:</label>
                                                <input type="hidden" name="quantity" id="quantity" value="<?php echo $order['quantity']; ?>" />
                                            <div id="quantitySlider"></div><br/><div id="qVal"><?php echo $order['quantity']; ?></div>
                                                <!--<input type="number" name="quantity" min="0" max='<?php echo $order['quantity']; ?>'>-->
                                                <input type="hidden" name="quantityOrdered" value="<?php echo $order['quantity']; ?>" />
                                                <input type="hidden" name="eid" value="<?php echo $row->equipmentID;?>" />
                                                <input type="hidden" name="loanPeriod" value="<?php echo $row->loanPeriod; ?>" />
                                                <input type="hidden" name="totalquantity" value="<?php echo $row->quantity;?>" />
                                            </p>
                                            <p><label for="pickup">Pick-up date:</label>
                                                <input type="text" id="datepicker"   name="startDate" value="<?php ?>"/>
                                            </p>
						<!-- required field for staff to enter content of the announcement -->
					
						
					    <p class="submitTwoBelow">
                                                <input type="hidden" name="orderid" value="<?php echo $order['orderID'];?>"/>
                                                
						<input type="submit" name="submitForm" value="Update" />
                                                <input type="submit" name="submitForm" value="Cancel" onClick='return ComfirmCancelOrder();' /> 
						   
					    </p>    
                                            
                                            <?php
                                            if ($this->session->flashdata('message')) {
                                            ?>
                                            <div>
                                                <?php echo $this->session->flashdata('message'); ?>
                                            </div>
                                            <?php
                                            
                                            }
                                        ?>
					</form>
				</div>
			</div>
		</div>

    </div>
</body>
</html>


