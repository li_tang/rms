<html>
<?php if($this->session->userdata('Category')!='Staff'){   redirect('login');}?>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bookings</title>
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('select#orderStatus').change(function(){
                    $(this).next('form').children('input#status').val($(this).val());
                }); 
                $('a.update').click(function(){
                    $(this).parent('td').prev('td').prev('td').children('form.updateStatus').submit();
                });
            })
        </script>
    </head>
    <body>
    <div id="wrapper">
     	<div id="content">
			<div id="content-inner">
                            <?php $this->load->view('templates/searchBarOrder');?>
				<div class="content-center">
		
                                        <div class="content-header"><h3>View Order</h3></div>			<br>
	 				<table class='regular'>
                                            <thead><th>Booking ID</th><th>Equipment ID</th><th>Start Date</th><th>End Date</th><th>Quantity</th><th>Order Status</th><th>Student</th><th>Edit</th></thead>
	        				<?php if($found){?>
		        				<?php foreach($booking as $booking_item): ?>  
									 <tr>
										<td><?php echo $booking_item['orderID'] ?></td>
										<td><?php echo $booking_item['equipmentID'] ?>
											 <td><?php echo $booking_item['startDate'] ?></td>
											 <td><?php echo $booking_item['endDate'] ?></td>
											 <td><?php echo $booking_item['quantity'] ?></td>
										<td>
											<select id="orderStatus">
												 <option value="Pending" <?php if ($booking_item['orderStatus']=='Pending') echo 'selected'; ?>>Pending</option>
												 <option value="Collected" <?php if ($booking_item['orderStatus']=='Collected') echo 'selected'; ?>>Collected</option>        
												 <option value="Expired" <?php if ($booking_item['orderStatus']=='Expired') echo 'selected'; ?>>Expired</option>
											</select>
											<form class="updateStatus" action="<?php echo site_url('orderManagementTeacher/UpdateStatus');?>" method="post">
												<input type='hidden' value ="<?php echo $booking_item['orderID']?>" name="orderid"/>
												<input type="hidden" id="status" name="status" value="<?php echo $booking_item['orderStatus']; ?>"/>
											</form>
										 </td>   
										 <td><?php echo $booking_item['studentID']?></td>
										 <td><a href="javascript:void(0);" class="update">Update</a></td>
									 </tr>
								<?php endforeach ?>
							<?php }else{ echo "<font color='red'>No record found !</font>";}?>
							<?php
								if ($this->session->flashdata('message')) {
								?>
								<div>
									<?php echo $this->session->flashdata('message'); ?>
								</div>
								<?php
								}
							?>
					</table>
				</div>
			</div>
		</div>
	</div>

    </body>
</html>
