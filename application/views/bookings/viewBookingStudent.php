<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
<?php if($this->session->userdata('Category')!='Student'){   redirect('login');}?>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bookings</title>
    </head>
    

    <body>
    <div id="wrapper">
     	<div id="content">
			<div id="content-inner">
				<div class="content-center">
					<div class="content-header"><h3>My Booking</h3></div>
	 				<table class='regular'>
	    				 <thead><th>Booking ID</th><th>Equipment ID</th><th>Start Date</th><th>End Date</th><th>Quantity</th><th>Order Status</th><th>Edit</th><th>Delete</th></thead>
	        				<?php foreach($booking as $booking_item): ?>  
	       					 <tr>
	    						 <td><?php echo $booking_item['orderID'] ?></td>
	    						 <td><?php echo $booking_item['equipmentID'] ?></td>
                                                         <td><?php echo $booking_item['startDate'] ?></td>
                                                         <td><?php echo $booking_item['endDate'] ?></td>
                                                         <td><?php echo $booking_item['quantity'] ?></td>
	      						 <td><?php echo $booking_item['orderStatus'] ?></td>
	  						 <td><?php echo anchor('orderManagement/updateBooking/'.$booking_item['orderID'],'Update'); ?></td>
	   						 <td><?php echo anchor('orderManagement/deleteOrder/'.$booking_item['orderID'],'Delete',  array('onClick' => "return confirm('Are you sure you want to delete?')")); ?></td>
                                                         
	    					 </tr>
							<?php endforeach ?>
					</table>
				</div>
			</div>
		</div>
	</div>
        
    </body>
</html>
