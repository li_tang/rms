<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    
    </head>
    <body>
    	<div id="wrapper">
        	<div id="content">
				<div id="content-inner">
					<div class="content-center">
						<div class="content-header"><h3>Login</h3></div>
						<br>
                                                <span class="errorMsg" id="loginErr"><?php echo validation_errors(); ?></span>
                                        <?php echo form_open(); ?>
    					<form action=<?php echo site_url('login/checkLogin');?> method="post">    
     						<p><label for="Usern"><b>UserID:</b></label>
     						<input type="text" size="20" id="UserID" name="UserID" value="<?php echo set_value('userID');?>"/>
     						</p>
     						<p><label for="Password"><b>Password:</b></label>
     						<input type="password" size="20" id="Passowrd" name="Password" value="<?php echo set_value('password'); ?>"/>
     						</p>
      						<input type="submit" value="Login" class="submitBelow" />
    					</form>
   					</div>
				</div>
			</div>
   		</div>
    </body>
</html>
