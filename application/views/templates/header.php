<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<base href="<?php echo base_url(); ?>">
        <title>Resource Management System | Home</title>
        <link rel="stylesheet" type="text/css" href=<?php echo base_url()."css/mystyle.css"?> />
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <link rel="stylesheet" type="text/css" href=<?php echo base_url()."css/select2.css"?> />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		 <script type="text/javascript" src="<?php echo base_url(); ?>js/select2.min.js"></script>
        <?php 
        	echo $library_src = (! isset($library_src)) ? $library_src = '' : $library_src;
    		echo $script_foot = (! isset($script_foot)) ? $script_foot = '': $script_foot;
        ?>
    
    </head>
    <body>
        <div id="wrapper">
		<div id="header">
			<div id="header-inner">
				<div id="header-img">
					<a href="http://www.uq.edu.au"><img src=<?php echo base_url()."images/uq-logo.gif"?> alt="UQ Logo"></a>
				</div>
				<div id="header-heading">
					<h1>Resource Management System</h1>
				</div>
			</div>
		</div>

