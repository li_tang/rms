<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<?php 
$startDate = $this->input->get('startDate', TRUE);
$studentID = $this->input->get('studentID', TRUE);
$orderStatus = $this->input->get('orderStatus', TRUE);
if($startDate==null){$startDate="";}
if($studentID==null){$studentID="";}
if($orderStatus==null){$orderStatus="";}
?>
<script>
	 $(function() {
	  $( "#datepicker" ).datepicker(
                  { dateFormat: "dd-mm-yy"}
            );
	  });
</script>
<div class="content-center">
<div class="content-header"><h3>Search Order</h3></div>
	<form action=<?php echo site_url('orderManagementTeacher/search');?> method = "get">
			<p>Start Date: <input type="text" id="datepicker" name="startDate" value="<?php echo $startDate;?>"></p>
			<p>Student ID: <input type="text" name="studentID" value="<?php echo $studentID;?>"></p>
			<p>Order Status: 
						<select name="orderStatus">
						   <option value="All" <?php if ($orderStatus=='All') echo 'selected'; ?>>All Status</option>
                           <option value="Pending" <?php if ($orderStatus=='Pending') echo 'selected'; ?>>Pending</option>
                           <option value="Collected" <?php if ($orderStatus=='Collected') echo 'selected'; ?>>Collected</option>        
                           <option value="Expired" <?php if ($orderStatus=='Expired') echo 'selected'; ?>>Expired</option>
                         </select>
	        </p>
	        <p><input type="submit" value="Search"></p>
	</form>
</div>