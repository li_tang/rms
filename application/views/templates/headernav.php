
   <div id="navbar">
	<div id="navbar-inner">
            <?php if ($this->session->userdata('Category')=='Student' ){?>
            <ul id="navbar-left">
				<li><?php echo anchor('home','Home');?></li>
                <!--<li><?php echo anchor('#','About');?></li>  -->
                <li><?php echo anchor('listEquipment','Equipment');?></li>
                <li><?php echo anchor('project','Project');?></li>
                <li><?php echo anchor('orderManagement','My Booking');?></li>
            </ul>
            <?php }else if($this->session->userdata('Category')=='Staff'){?>
            <ul id="navbar-left">
                <li><?php echo anchor('announcement','Announcement');?></li>
                <li><?php echo anchor('listEquipment','Equipment');?></li>
                <li><?php echo anchor('orderManagementTeacher','Manage Order');?></li>
            </ul>
            <?php }?>
            <ul id="navbar-right">
				<li><span class="notLink">Hi <?php echo $this->session->userdata('UserID');?></span></li>
                <li><?php echo anchor('login/logout/','Logout');?></li>
            </ul>
	</div>
    </div>
