<?php 
$searchStr = $this->input->get('searchEquipment', TRUE);
$searchOpt = $this->input->get('searchOpt', TRUE);
if($searchStr==null){$searchStr="";}
if($searchOpt==null){$searchOpt="";}
 ?>
<div class="content-center">
	<div class="content-header"><h3>Search Equipments</h3></div>
	<form action=<?php echo site_url('listEquipment/search');?> method = "get">
		<p><input type="text" name="searchEquipment" value="<?php echo $searchStr;?>">&nbsp;&nbsp;<input type="submit" value="Search"></p>
		<p>
			<input type="radio" name="searchOpt" value="equipmentName" id="equipmentName" <?php if($searchOpt=="equipmentName"){echo "checked";}?> checked="checked">
			<label for="name" >Equipment Name</label>
			<?php if($this->session->userdata('Category')=="Student"){?>
			<input type="radio" name="searchOpt" value="projectName" id="project" <?php if($searchOpt=="projectName"){echo "checked";}?>>
			<label for="project" class="smallerLabel">Project Name</label>
			<input type="radio" name="searchOpt" value="course" id="course" <?php if($searchOpt=="course"){echo "checked";}?>>
			<label for="course" class="smallerLabel">Course Code</label>
			<?php }?>
			<?php if($this->session->userdata('Category')=="Staff"){?>
			<input type="radio" name="searchOpt" value="equipmentID" id="equipmentID" <?php if($searchOpt=="equipmentID"){echo "checked";}?>>
			<label for="name" class="smallerLabel">Equipment ID</label>
			<?php }?>
		</p>
	</form>
</div>