<div id="content">
	<div id="content-inner">
		<div class="content-center">
			<div class="content-header"><h3>Manage Project</h3></div>
			<table table border="1" cellpadding="2" cellspacing="1" class="regular">
				<tr>
				<th>Project ID</th>
				<th>Title</th>
				<th>Course Code</th>
				<th>Image</th>
				<th>Edit Project</th> 
				</tr>
		
				  <?php  foreach($query as $row): ?>
				        
				      
				        <tr>
				        	
				        	<td><?=$row->projectID;?></td>
				            <td><?=$row->title;?></td>
				            <td><?=$row->coursecode;?></td>
				            <?php if($row->filecontent != null){?>
				            	<td><?php echo '<img src="data:'.$row->filetype.';base64,' . base64_encode( $row->filecontent ) .'" height="100" width="100"/>';?></td>
				            <?php } else {?> 
				            	<td><?php echo '<img src="'.base_url().'images/noimage.gif" alt="No Image" height="100" width="100">';?></td>
				            <?php }?>
				            <td><?php echo anchor( "modifyProjectForm/index/".$row->projectID, 'Edit')?></td>
				        </tr>
				  <?php endforeach;?>
 			</table>
 
 

 

  
</div>
</div>
</div>