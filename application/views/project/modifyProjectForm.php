<script>

	$(document).ready(function() { 
		$("#equipmentList").select2(); 
		$("#equipmentList").on("change", function() { 
			$("#result").text($("#equipmentList").val()); //displaying the result in result div
			$("#equipmentListResult").val($("#equipmentList").val()); //actually putting the result in hidden input
		});
		
	});
		
</script>

		<div id="content">
			<div id="content-inner">
				<div class="content-center">
					<div class="content-header"><h3>Edit Project</h3></div>
					<br>
					<?php echo validation_errors(); ?>
<?php foreach($query as $row){?>
					
<form action=<?php echo site_url('modifyProjectForm/update_to_db');?> method="post" enctype="multipart/form-data">

						<p><label for="projectID"></label>
							<input name="projectID" class="element text medium" type="hidden" maxlength="255" value="<?php echo $row->projectID;?>"/> 
						</p>
						
						<p><label for="projectname">Project Name:</label>
							<input id="element_1" name="projectName" class="element text medium" type="text" maxlength="255" value="<?php echo $row->title;?>" required/> 
						</p>
						
						<p><label for="image">Image:</label>
							
                			<input type="hidden" name="image" />
                			Choose a file to upload: <input name="uploadedfile[]" type="file" accept="image/*" /><br />

						</p>
						<p><label for="coursecode">Course code:</label>
							<select name="coursecode">
								<option value="<?php echo $this->session->userdata('course1')?>"><?php echo $this->session->userdata('course1')?></option>
							    <option value="<?php echo $this->session->userdata('course2')?>"><?php echo $this->session->userdata('course2')?></option>
							    <option value="<?php echo $this->session->userdata('course3')?>"><?php echo $this->session->userdata('course3')?></option>
							    <option value="<?php echo $this->session->userdata('course4')?>"><?php echo $this->session->userdata('course4')?></option>
							</select>
						</p>
						
						<p><label for="contentarea">Description:</label>
						   <textarea type="input" name="contentarea"><?php echo $row->description;?></textarea>
                                                   <script type="text/javascript">
			                             CKEDITOR.replace( 'contentarea' );
			                    	</script>
						</p>
						
						
						
						<!--<p><div id="equipmentBoxes">
							<label for="equipment1">Equipment 1: </label>
							<input id="equipment1" type="text" class="equipmentList"/>
							<input id="equipment1ID" type="hidden" value=""/>
						</div>
						<input type="button" onclick="addMoreEquipment()" value="Add more equipment"></p>  --> 
						<p><label for="equipmentList">Equipments:</label>
							<select id="equipmentList" multiple>
								<?php foreach($tableContent as $rowa){
										if (in_array($rowa->equipmentID, $equipmentList)){?>
										<option value=<?php echo $rowa->equipmentID;?> selected>
									<?php } else {?>
										<option value=<?php echo $rowa->equipmentID;?>>
									<?php }?>
										<?php echo $rowa->equipmentName?>
									</option>
							<?php }?>
							</select>
						</p>
						<input type="hidden" value="" id="equipmentListResult" name="equipmentListResult">
<!-- 						<div id="result"></div>				 -->
						
						<p><label for="studentID"></label>
							<input name="studentID" class="element text medium" type="hidden" readonly maxlength="255" value="<?PHP echo $this->session->userdata('UserID') ?>"/>
						</p>
						
						<p><input class="submitBelow" type="submit" value="Update"></p>
					</form>

    		</div>
			</div>
		</div>
		<?php }?>