<div id="content">
    <div id="content-inner">
	        <?php 
			$searchStr = $this->input->get('searchProject', TRUE);
			$searchOpt = $this->input->get('searchOpt', TRUE);
			if($searchStr==null){$searchStr="";}
			if($searchOpt==null){$searchOpt="";}
			 ?>
			<div class="content-center">
				<div class="content-header"><h3>Search Project</h3></div>
				<form action=<?php echo site_url('project/search');?> method = "get">
					<p><input type="text" name="searchProject" value="<?php echo $searchStr;?>">&nbsp;&nbsp;<input type="submit" value="Search"></p>
					<p>
						<input type="radio" name="searchOpt" value="projectName" id="project" <?php if($searchOpt=="projectName"){echo "checked";}?> checked="checked">
						<label for="project" class="smallerLabel">Project Name</label>
						<input type="radio" name="searchOpt" value="course" id="course" <?php if($searchOpt=="course"){echo "checked";}?>>
						<label for="course" class="smallerLabel">Course Code</label>
						<!--  <input type="radio" name="searchOpt" value="equipmentName" id="project" <?php if($searchOpt=="projectName"){echo "checked";}?>>
						<label for="project" class="smallerLabel">Equipment Name</label>-->
					</p>
				</form>
			</div>
			<div class="content-left">
				<div class="content-center">
					<div class="content-header"><h3>My Courses</h3></div>
					<ul class="link-list">
					<?php $course1 = $this->session->userdata('course1');
						  $course2 = $this->session->userdata('course2');
						  $course3 = $this->session->userdata('course3');
						  $course4 = $this->session->userdata('course4');
					?>
						<li><a href=<?php echo site_url("project/search?searchProject=".$course1."&searchOpt=course");?>><?php echo "[".$course1."]";?></a></li>
						<li><a href=<?php echo site_url("project/search?searchProject=".$course2."&searchOpt=course");?>><?php echo "[".$this->session->userdata('course2')."]";?></a></li>
						<li><a href=<?php echo site_url("project/search?searchProject=".$course3."&searchOpt=course");?>><?php echo "[".$this->session->userdata('course3')."]";?></a></li>
						<li><a href=<?php echo site_url("project/search?searchProject=".$course4."&searchOpt=course");?>><?php echo "[".$this->session->userdata('course4')."]";?></a></li>
					</ul>
				</div>
			
	            
	            <div class="content-center">
	                <div class="content-header"><h3>Tools</h3></div>
	                 <ul class="link-list">
	                     <li><?php echo anchor('addProject','Create New Project');?></li>
	                    <li><?php echo anchor('modifyProject','Manage Your Project');?></a></li>
	                </ul>
	            </div>
	        </div>
	        
	        <div class="content-right">
	        
	            <div class="content-center">
	                <div class="content-header"><h3>Search Project</h3></div>
	                 <?php if ($tableContent != null){?>
	                	<ul class="result-list">                
		                <?php foreach($tableContent as $row){?>
							<li>
								<a href=<?php echo site_url("viewProject/index/".$row->projectID);?>>
								<?php if ($row->filecontent == null){?>
									<div class="result-img"><img src=<?php echo base_url()."images/noimage.gif"?> alt="Noimage"></div>
								<?php } else {?>
									<div class="result-img"><img src='data:<?php echo $row->filetype?>;base64,<?php echo base64_encode( $row->filecontent )?>'></div>
								<?php }?>
									<div class="result-desc">									
										<h3><?php echo $row->title;?></h3>
										<p><?php echo $row->description;?></p>
										<p>Course: <?php echo $row->coursecode;?></p>
										<p><font color='green'>Likes: <?php echo $row->likeNum;?></font></p>
									</div>
								</a>
							</li>
						<?php }?>
					</ul>	
					<?php } else echo "<font color='red'>No projects found</font>";?> 
	                <?php //echo $this->table->generate($tableContent); ?>
	                <?php echo $this->pagination->create_links();?>
	            </div>
	        </div>
	
    </div>
</div>    

