<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<div id="content">
	<div id="content-inner">
		<div class="content-center detail-desc"><br/>
			
			<?php echo $this->table->generate($projectDetail); ?>           
		</div>
		<div class="content-center">
			<div class="content-header"><h3>Equipment Used</h3></div>
			<?php foreach ($equipmentList as $row){ ?>
				<div class="detail-related-link">
				<?php if ($row->filecontent == null)
						echo anchor('viewEquipment/index/'.$row->equipmentID,'<img class="detail-related" src="'.base_url().'images/noimage.gif" alt="No Image">'. $row->equipmentName);
					else 
						echo anchor('viewEquipment/index/'.$row->equipmentID,'<img class="detail-related" src="data:'.$row->filetype.';base64,' . base64_encode( $row->filecontent ) . '">'. $row->equipmentName);
				?>
				</div>
			<?php }?>			
		</div>
			<div class="content-center content-full">
				<div class="content-header"><h3>Comments</h3></div>
					<div id="wrap">
					      <div class="container">
					        <div class="comment-list">
					        		<?php if($comment_for_project): ?>
			        		<h3>Comment (<?php echo count($comment_for_project); ?>)</h3>
			        		<ul>
			        			<?php foreach ($comment_for_project as $key => $comment): ?>
			        			<li class="row comment-item" id="comment-<?php echo $comment['commentID']; ?>">
			        				<img src="http://www.gravatar.com/avatar/?d=mm" alt="" class="img-circle pull-left">
			        				<div class="comment-content">
			        					<div class="comment-author rows">
			        						<strong><?php echo $comment['studentID']; ?></strong>
			        						<span class="comment-time pull-right">
			        							<?php echo date('F j, Y - g:i a', $comment['created_time']); ?>
			        						</span>
			        					</div>
			        					<span><?php echo $comment['content']; ?></span>
			        					<span class="commentID"><?= $comment['commentID'] ?></span>
							        	<button class="comment-vote pull-right">Like</button>
							        	<span class="count pull-right"><?php echo $comment['likes'];?></span>
			        				</div>
			        			</li>
			        			<?php endforeach; ?>
			        		</ul>
			        		<?php else: ?>
			        		<h3>Comment (There are no comments yet!)</h3>
			        		<?php endif; ?>
			        	</div>
			
			        	<div class="comment-form">
			        		<?php echo $form; ?>
								<fieldset>
									<legend>Leave a comment</legend>
			
									<label>Your comment *</label>
									<textarea name="comment"></textarea>
									<script type="text/javascript">
			                             CKEDITOR.replace( 'comment' );
			                    	</script><br/>
			
									<input type="submit" class="btn btn-success submit-comment" value="Submit">
									<span class="notice"></span>
								</fieldset>
							</form>
			        	</div>	
			      	</div>
			    </div>
		  </div>
	</div>
</div>
<!-- include js&css file for viewProject-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/customise.css" />
<script src="<?php echo base_url().'assets/js/scriptforproject.js'; ?>"></script>
