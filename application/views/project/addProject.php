<script>



	$(document).ready(function() { 
		$("#equipmentList").select2(); 
		$("#equipmentList").on("change", function() { 
			$("#result").text($("#equipmentList").val()); //displaying the result in result div
			$("#equipmentListResult").val($("#equipmentList").val()); //actually putting the result in hidden input
		});
		
	});
		
</script>

		<div id="content">
			<div id="content-inner">
				<div class="content-center">
					<div class="content-header"><h3>Add Project</h3></div>
					<br>
					<?php echo validation_errors(); ?>
<form action=<?php echo site_url('addProject/insert_to_db');?> method="post" enctype="multipart/form-data">
						<p><label for="projectname">Project Name:</label>
							<input id="element_1" name="projectName" class="element text medium" type="text" maxlength="255" value=""/> 
						</p>
						
						<p><label for="image">Image:</label>
							
                			<input type="hidden" name="image" />
                			Choose a file to upload: <input name="uploadedfile[]" type="file" accept="image/*" /><br />

						</p>
						<p><label for="coursecode">Course code:</label>
							<select name="coursecode">
								<option value="<?php echo $this->session->userdata('course1')?>"><?php echo $this->session->userdata('course1')?></option>
							    <option value="<?php echo $this->session->userdata('course2')?>"><?php echo $this->session->userdata('course2')?></option>
							    <option value="<?php echo $this->session->userdata('course3')?>"><?php echo $this->session->userdata('course3')?></option>
							    <option value="<?php echo $this->session->userdata('course4')?>"><?php echo $this->session->userdata('course4')?></option>
							</select>
						</p>
						
						<p><label for="contentarea">Description:</label>
						   <textarea name="contentarea" required></textarea>
                                                   <script type="text/javascript">
			                             CKEDITOR.replace( 'contentarea' );
			                    	</script>
						</p>
						
						
						<p><label for="equipmentList">Equipments:</label>
							<select id="equipmentList" multiple>
								<?php foreach($tableContent as $row){?>
								<option value=<?php echo $row->equipmentID;?>>
								<?php echo $row->equipmentName?>
								</option>
							<?php }?>
							</select>
						</p>
						<input type="hidden" value="" id="equipmentListResult" name="equipmentListResult">
<!-- 						<div id="result"></div>				 -->
						
						<p><label for="studentID"></label>
							<input name="studentID" class="element text medium" type="hidden" readonly maxlength="255" value="<?PHP echo $this->session->userdata('UserID') ?>"/>
						</p>
						
						<p><input class="submitBelow" type="submit" value="Create"></p>
					</form>

    		</div>
			</div>
		</div>