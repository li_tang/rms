<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* @author Andrew
* controller class for handling add equipment detail page
*/
class AddProject extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('project_model');
		}
		else
		{
			redirect('login');
		}
	}
	
	function index()
	{	
		//get the equipment list from model
		$this->load->model('equipment_model');
		$data["tableContent"] = $this->equipment_model->get_equipment_list();
		
        // Load templates
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
        // loading form view
		$this->load->view('project/addProject.php', $data);
		$this->load->view('templates/footer');
	}
	/**
	 * @author Andrew
	 * Insert into database
	 */
	function insert_to_db()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_error_delimiters('<div class="errorMsg">', '</div>');
		
		$this->form_validation->set_rules('projectName', 'Project Name', 'trim|required|max_length[50]|xss_clean');
		$this->form_validation->set_rules('contentarea', 'Description', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->model('equipment_model');
			$data["tableContent"] = $this->equipment_model->get_equipment_list();
			
			// Load templates
			$this->load->view('templates/header');
			$this->load->view('templates/headernav');
			// loading form view
			$this->load->view('project/addProject', $data);
			$this->load->view('templates/footer');
		}
		else
		{
				// Load the project-model
			$this->load->model('project_model');
			$projectID = $this->project_model->insert_into_db();
			if ($_POST['equipmentListResult'] != null || $_POST['equipmentListResult'] != ""){
				$equipmentList = explode ( "," ,$_POST['equipmentListResult']);
				$this->project_model->insert_equipment_project_relation($equipmentList,$projectID);
			}
	        redirect('project/project');
		}
        
	}
	
}

