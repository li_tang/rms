<?php
class orderManagementTeacher extends CI_Controller {
    //put your code here
    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('order_model');
		}
		else
		{
			redirect('login');
		}
	}

	public function index()
	{       
                 
		//$studentID  = $this->session->userdata('UserID');
		//echo var_dump($this->order_model->get_Allbookings());
		$data['booking'] = $this->order_model->get_Allbookings();
		$data['found'] = true;
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('bookings/viewBookingStaff', $data);
		$this->load->view('templates/footer');
	}
        
	public function UpdateStatus(){
		$id = $this->input->post('orderid');
		$data = array('orderStatus'=>$this->input->post('status'));
		$status = $this->order_model->set_bookingByOrderID($id,$data);
			
	    if($status ==1)
	    {
			$this->session->set_flashdata("message","Record Successfully Updated!");
			redirect('orderManagementTeacher/index');
	    }
	    else{
			$this->session->set_flashdata("message","Record Not Updated!");
			redirect('orderManagementTeacher/index');
	    }
	}
	
	public function deleteOrder($orderID)
	{
		$this->order_model->delete_Order($orderID);
		redirect('orderManagementTeacher');
	}
	
	public function search()
	{
		$startDate = $this->input->get('startDate', TRUE);
		if($startDate!='')
		{
			$startDate = date("Y-m-d", strtotime($startDate));
		}
		$studentID = $this->input->get('studentID', TRUE);
		$orderStatus = $this->input->get('orderStatus', TRUE);
		$data['found'] = false;
		$data['booking'] = $this->order_model->search_order($startDate,$studentID,$orderStatus);
		if(count($data['booking'])>0)
		{
			$data['found'] = true;
		}
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('bookings/viewBookingStaff', $data);
		$this->load->view('templates/footer');
	}
}

?>
