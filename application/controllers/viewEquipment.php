<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* @author li.tang yanbo.zhao
* controller class for handling view equipment detail page
*/
class ViewEquipment extends CI_Controller {
    public function __construct()
	{
		parent::__construct();
		$this->jquery->script(base_url().'js/jquery-1.9.1.js',TRUE);
		$this->load->model('equipment_model');
		$this->equipment_model->java_function();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('equipment_model');
		}
		else
		{
			redirect('login');
		}
	}
	
	public function index($id='')
	{
		//checking whether page exist
		if ( ! file_exists('application/views/equipment/viewEquipment.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		// loading libraries for this page
		$this->load->library("table");
		$this->load->model("equipment_model");
		
		
		//process query
		$data["equipmentDetail"] = $this->equipment_model->get_equipment_by_criteria(array('equipmentID' => $id));
		$recommendedEquipment = $this->equipment_model->get_recommended_equipment($id);
		
		$data["recommendedEquipment"] = $recommendedEquipment;
		
		$id = (int) $id;
		//load url helper
		$this->load->helper('url');
		if($id == '')
			redirect(base_url());
		//load form helper
		$this->load->helper('form');
		//load post model
		$this->load->model('equipment_model', 'post');
		
		//get comment for certain equipment by equipmentID
		$data['comment_for_equipment'] = $this->post->get_comments($id);
		$data['form'] = form_open('equipment/viewEquipment/index', array('id' => 'comment_form'), array('equipmentID' => $id));
		
		//create like method
		$data['likes'] = $this->equipment_model->get_likes();
		if($this->input->post('likes'))
		{
			$commentID = $this->input->post('commentID');
			$likes = $this->input->post('likes');
			$this->equipment_model->add_likes($commentID,$likes);
		}
				
		//rendering view
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('equipment/viewEquipment',$data);
		$this->load->view('templates/footer');
	}
	
	/**
	 * @author yanbo.zhao
	 * function to submit comment by ajax without refreshing the page
	 */
	public function ajax_submit()
	{
		//Check if the request is ajax request
		if($this->input->is_ajax_request()) 
		{
			//because we use ajax with method post, so we need to use function post in input class.
			//get id of the current post.
			$data['equipmentID']        = (int) $this->input->post('equipmentID'); 
			$data['studentID']    = htmlspecialchars($this->session->userdata('UserID'));
			$data['content']        = $this->input->post('comment');
			$data['created_time']   = time();
			$data['likes'] =(int) $this->input->post('likes');
				
			//equipmentID cannot be null
			if($data['equipmentID'] == '')
			{
				echo json_encode(array('err' => 1, 'content' => 'Oops, The post is not available yet!'));
				//stop the action
				die; 
			}
	
			//comment content cannot be null
			if($data['content'] == '')
			{
				echo json_encode(array('err' => 1, 'content' => '* fields cannot be empty!'));
				die;
			}
	
			//insert the comment
			$this->load->model('equipment_model', 'post');
			if($this->post->insert('comment_for_equipment', $data))
			{
				//create new comment element to insert to template
				$author = '<strong>'.$data['studentID'].'</strong>';
	
				$new_comment = '
				<li class="row comment-item" id="comment-'.$data['equipmentID'].'">
        				<img src="http://www.gravatar.com/avatar/?d=mm" alt="" class="img-circle pull-left">
        				<div class="comment-content">
        					<div class="comment-author rows">
        						'.$author.'
        						<span class="comment-time pull-right">'.date('F j, Y - g:i a', $data['created_time']).'</span>
        					</div>
        					<span>'.$data['content'].'</span>
        					<button class="comment-vote pull-right">Like</button>
        					<span class="count pull-right">'.$data['likes'].'</span>
        				</div>
        		</li>';
				//display thank you message if successfully submit comment
				echo json_encode(array('err' => 0, 'new_comment' => $new_comment));
				die;
			}
			//else display try again message if cannot submit
			else
			{
				echo json_encode(array('err' => 1, 'content' => 'Oops! We cannot get your comment at this time, pls try again later!'));
				die;
			}
		}
	}
}