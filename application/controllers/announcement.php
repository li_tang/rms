<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author WANG QI
 * 
 * 
 * 
 */
class Announcement extends CI_Controller {
    
    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('announcement_model');
		}
		else
		{
			redirect('login');
		}
	}

	public function index()
	{
		$data['announcement'] = $this->announcement_model->get_announcement();
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('announcement/index', $data);
		$this->load->view('templates/footer');

	}
       
	/**
	 * view annoucement
	 * @param unknown $annID announcement ID
	 */
	public function view($annID)
	{
		$data['announcement_item'] = $this->announcement_model->get_announcement($annID);
		if (empty($data['announcement_item']))
		{
			show_404();
		}
		$data['annID']=$data['announcement_item']['annID'];
		$data['content'] = $data['announcement_item']['content'];
		$data['title']=$data['announcement_item']['title'];
		$data['staffID']=$data['announcement_item']['staffID'];
		$data['date']=$data['announcement_item']['date'];
		 // Load the header and view the announcement page
		 $this->load->view('templates/header');
		 $this->load->view('templates/headernav');
		 $this->load->view('announcement/view', $data);
		 $this->load->view('templates/footer');

	}
       
    public function createAnnouncement()
	{   
		$formSubmit = $this->input->post('submitForm');
		if( $formSubmit == 'Cancel')
		{
			redirect('announcement');
		}
		else{
			//load form helper from codeIgniter
			$this->load->helper('form');
			$this->load->library('form_validation');
		
			//generate form required fields
			$this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[50]|xss_clean');
			$this->form_validation->set_rules('contentarea', 'Description', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('date', 'date', 'required');
		
			//rendering form layout for staff to type in
			if ($this->form_validation->run() === FALSE)
			{
				$this->load->view('templates/header');
				$this->load->view('templates/headernav');
				$this->load->view('announcement/createAnnouncement');
							 $this->load->view('templates/footer');

			}
			else
			{
				//load model to insert announcement into database
				$this->announcement_model->set_announcement();
				//After successfully create email, redirect to home page
							 redirect('announcement');
			}
		}
	}
	
	public function updateAnnouncement($annID)
	{	
		$formSubmit = $this->input->post('submitForm');
		if( $formSubmit == 'Cancel' )
		{
			redirect('announcement');
		}
		else{
			$data['announcement_item'] = $this->announcement_model->get_announcement($annID);
			if (empty($data['announcement_item']))
			{
				show_404();
			}
			//load form helper from codeIgniter
			$this->load->helper('form');
			$this->load->library('form_validation');
		
			//generate form required fields
			$this->form_validation->set_rules('title', 'title', 'trim|required|max_length[50]|xss_clean');
			$this->form_validation->set_rules('contentarea', 'contentarea', 'trim|required|xss_clean');
			$this->form_validation->set_rules('date', 'date', 'required');
		
			//rendering form layout for staff to type in
			if ($this->form_validation->run() === FALSE)
			{
				$this->load->view('templates/header');
				$this->load->view('templates/headernav');
				$this->load->view('announcement/updateAnnouncement',$data);
									$this->load->view('templates/footer');
			}
			else
			{		
				$this->announcement_model->update_announcement();
				redirect('announcement');
			}
		}
	}
	
	public function deleteAnnouncement($annID)
	{
		$this->load->view('templates/header');
		$this->load->view('announcement/deleteAnnouncement',$annID);
		$this->load->view('templates/footer');
		$formSubmit= $this->input->post('confirmForm');
		if( $formSubmit == 'Yes' )
		{ 
			$this->announcement_model->delete_announcement($annID);
			redirect('announcement');
		}
		if($formSubmit=='No')
		{
			redirect('announcement');
		}
	}
}
?>
