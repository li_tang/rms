<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @author LIU XINLU
 * This controller contains insert order method, which gets value from makeorder view page 
 * This controller contains email method, which will send user a confirmation email after he made bookings.
 */
class orderManagement extends CI_Controller 
{
     public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('order_model');
		}
		else
		{
			redirect('login');
		}
	}

	public function index()
	{       
		$studentID  = $this->session->userdata('UserID');
		$arr = $this->order_model->get_bookingByStudentID($studentID);
		//$studentID  = $this->session->userdata('UserID');
		//echo var_dump($arr)
		$data['booking'] = $arr;
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('bookings/viewBookingStudent', $data);
		$this->load->view('templates/footer');
	}
        
	public function UpdateBooking($orderID)
	{
		$this->load->model('order_model');
		$this->load->model('equipment_model');
		$orderArr = $this->order_model->get_bookingByOrderID($orderID);
		$equipmentArr = $this->equipment_model->get_equipment_info($orderArr['equipmentID']);
		//echo var_dump($this->equipment_model->get_equipment_info($orderArr['equipmentID']));
		$data['order'] = $orderArr;
		$data['equipment'] = $equipmentArr;
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('bookings/updateBookingStudent', $data);
		$this->load->view('templates/footer');
	}

	/**
	 * caculate end date based on collection date and loan duration
	 * @param  $date collection date
	 * @param  $days loan duration
	 */
	function addDaysWithDate($date,$days){
		$date = strtotime("+".$days." days", strtotime($date));
		return  date("Y-m-d", $date);
	}
        
	function update_quantity()
	{
		$this->load->model('equipment_model');
		$id = intval($this->input->post('eid'));
		$quantityStock=intval($this->input->post('totalquantity'));
		$quantityOrder= intval($this->input->post('quantityOrdered'));
		$quantityNew=intval($this->input->post('quantity'));
		$quantitySum=0;
		if($quantityNew>=$quantityOrder)
		{
			$quantitySum = $quantityStock-($quantityNew-$quantityOrder);
		}
		else if($quantityNew<$quantityOrder)
		{
			$quantitySum = $quantityStock+($quantityOrder-$quantityNew);
		}
		
		$this->equipment_model->update_quantity($quantitySum,$id);
	}
        
	function addBackQuantity()
	{
		$this->load->model('equipment_model');
		$id = intval($this->input->post('eid'));
		$quantityStock=intval($this->input->post('totalquantity'));
		$quantityOrder= intval($this->input->post('quantityOrdered'));
		
		$quantitySum = $quantityStock+$quantityOrder;
		
		$this->equipment_model->update_quantity($quantitySum,$id);
	}
        
	public function UpdateOrder()
	{
		$id = $this->input->post('orderid');
		$orderRowArr = $this->order_model->get_bookingByOrderID($id);
		$this->load->model('equipment_model');
		$equipArr = $this->equipment_model->get_equipment_info($orderRowArr['equipmentID']);
		//echo $equipArr[0]->loanPeriod;
		$startDate=$this->input->post('startDate');
		$CollDate = str_replace('/', '-', $startDate);
		$endDate= $this->addDaysWithDate($CollDate, intval($equipArr[0]->loanPeriod));

		$data = array('startDate'=>date('Y-m-d', strtotime($CollDate)),'endDate'=>$endDate,'quantity'=>$this->input->post('quantity'));
		
		$this->update_quantity();
		
	    if($this->order_model->set_bookingByOrderID($id,$data))
	    {
			$this->session->set_flashdata('message',"Record Updated!" ); 
			redirect('orderManagement/index');
	    }
	   else{
			$this->session->set_flashdata("message","Record Not Updated!");
			redirect('orderManagement/index');
	    }
	}
        
	public function deleteOrder($orderID)
	{
		$this->load->model('order_model');
		$this->load->model('equipment_model');
		$orderArr = $this->order_model->get_bookingByOrderID($orderID);
		$equipmentArr = $this->equipment_model->get_equipment_info($orderArr['equipmentID']);
		
		$quantityStock=intval($orderArr['quantity']);
		$quantityOrder= intval($equipmentArr[0]->quantity);
		 
		$quantitySum = $quantityStock + $quantityOrder;
		$this->equipment_model->update_quantity($quantitySum,$equipmentArr[0]->equipmentID);
		$this->order_model->delete_Order($orderID);
		redirect('orderManagement');
	}  
}