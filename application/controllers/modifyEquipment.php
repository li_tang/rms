<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class ModifyEquipment extends CI_Controller {
    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('equipment_model');
		}
		else
		{
			redirect('login');
		}
	}
	
	public function index($id='')
	{
		//checking whether page exist
		if ( ! file_exists('application/views/equipment/viewEquipment.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		// loading libraries for this page
		$this->load->library("table");
		$this->load->model("equipment_model");
		
		
		//process query
		$data["equipmentDetail"] = $this->equipment_model->get_equipment_by_criteria(array('equipmentID' => $id));
		
		$id = (int) $id;
		
		//load url helper
		$this->load->helper('url');
		if($id == '')
			redirect(base_url());
		//load form helper
		$this->load->helper('form');
		//load post model
		$this->load->model('equipment_model', 'post');
		
		
		//rendering view
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('equipment/modifyEquipment',$data);
		$this->load->view('templates/footer');
	}
			
	
	/**
	 * @author Andrew
	 * Insert into database
	 */
	
	function edit_to_db()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_error_delimiters('<div class="errorMsg">', '</div>');
		
		$this->form_validation->set_rules('equipmentName', 'Equipment Name', 'trim|required|max_length[50]|xss_clean');
		$this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
		$this->form_validation->set_rules('manufacturer', 'Manufacturer', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE){
			// loading libraries for this page
			$this->load->library("table");
			$this->load->model("equipment_model");
			
			$id = (int) $_POST['equipmentID'];
			
			//process query
			$data["equipmentDetail"] = $this->equipment_model->get_equipment_by_criteria(array('equipmentID' => $id));
			
			//load url helper
			$this->load->helper('url');
			if($id == '')
				redirect(base_url());
			//load post model
			$this->load->model('equipment_model', 'post');
	
			//rendering view
			$this->load->view('templates/header');
			$this->load->view('templates/headernav');
			$this->load->view('equipment/modifyEquipment',$data);
			$this->load->view('templates/footer');
		} else {
	        // Load the equipment-model
			$this->load->model('equipment_model');
			$this->equipment_model->edit_into_db();
			// Call the listEquipmentStaff controller
	        redirect('listEquipment');
		}
	}
	
}

	