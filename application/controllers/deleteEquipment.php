<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/** 
* @author Andrew
* controller class for handling delete equipment detail page
*/
class DeleteEquipment extends CI_Controller {
    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('equipment_model');
		}
		else
		{
			redirect('login');
		}
	}
	
	public function index($id)
	{
		//checking whether page exist
		if ( ! file_exists('application/views/equipment/deleteEquipment.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		// loading libraries for this page
		/*$this->load->library("table");
		$this->load->model("equipment_model");
		
		//process query
		$data["equipmentDetail"] = $this->equipment_model->delete_equipment_by_id($id);
		
		
		//rendering view
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
        //loading delete success view
		$this->load->view('equipment/deletesuccess');
		$this->load->view('templates/footer');*/
		
		$this->load->model("equipment_model");
		
		$this->load->view('templates/header');
		$this->load->view('equipment/deleteEquipment',$id);
		$this->load->view('templates/footer');
		$formSubmit= $this->input->post('confirmForm');
		if( $formSubmit == 'Yes' )
		{
			$this->equipment_model->delete_equipment_by_id($id);;
			redirect('listEquipment');
		}
		if($formSubmit=='No')
		{
			redirect('listEquipment');
		}
	}
}