<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of viewProject
 *
 * @author Haku  yanbo.zhao
 */
class ViewProject extends CI_Controller{
    public function __construct()
	{
		parent::__construct();
		$this->jquery->script(base_url().'js/jquery-1.9.1.js',TRUE);
		$this->load->model('project_model');
		$this->project_model->java_function();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('project_model');
		}
		else
		{
			redirect('login');
		}
	}
	
    public function index($id)
	{
		/*/checking whether page exist
		if ( ! file_exists('application/views/project/viewProject.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}*/
		// loading libraries for this page
		$this->load->library("table");
		$this->load->model("project_model");
		
		//process query
		$data["projectDetail"] =  $this->_processList($this->project_model->get_project_by_id(array('projectID' => $id)));
		$data["equipmentList"] =  $this->project_model->get_equipment_project($id);
		
		$id = (int) $id;
		//load url helper
		$this->load->helper('url');
		if($id == '')
			redirect(base_url());
		//load form helper
		$this->load->helper('form');
		//load post model
		$this->load->model('project_model', 'post');
		
		//get comment for certain project by projectID
		$data['comment_for_project'] = $this->post->get_comments($id);
		
		$data['form'] = form_open('project/viewProject/index', array('id' => 'comment_form'), array('projectID' => $id));
		
		//create like method
		$data['likes'] = $this->project_model->get_likes();
		if($this->input->post('likes'))
		{
			$commentID = $this->input->post('commentID');
			$likes = $this->input->post('likes');
			$this->project_model->add_likes($commentID,$likes);
		}
		
		//rendering view
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('project/viewProject',$data);
        $this->load->view('templates/footer');
	}
        
    /**
     * process query result so that it can be displayed on the view page
     * @param unknown $queryResult query result from database
     */
    private function _processList($queryResult)
	{	
		$this->load->helper('html');
		$attributes = array(
							'class' => 'result-list',
							);
		
		foreach($queryResult as $row)
		{
			if ($row->filecontent == null){
				$desc = "<img class='detail-img' src='".base_url()."images/noimage.gif' alt='No Image'>".
					"<ul class='detail-list'></li><h3>".$row->title."</h3></li>".
					"<li>".$row->description."</li>".
					"<li>Student: ".$row->studentID."</li>".
					"<li>Likes: ".$row->likeNum."</li>".
					"<li>".anchor('viewProject/like_project/'.$row->projectID,'Like This')."</ul>";
			}
			else {
				$desc = "<img class='detail-img' src='data:".$row->filetype.";base64," . base64_encode( $row->filecontent ) . "'>".
						"<ul class='detail-list'></li><h3>".$row->title."</h3></li>".
						"<li>".$row->description."</li>".
						"<li>Student: ".$row->studentID."</li>".
						"<li>Likes: ".$row->likeNum."</li>".
						"<li>".anchor('viewProject/like_project/'.$row->projectID,'Like This')."</ul>";
			}
			$this->table->add_row($desc);
		}
    }
     
    public function like_project($id){
     	$this->project_model->like_project($id);
     	redirect('/viewProject/index/'.$id, 'refresh');
    }
        
	public function ajax_submit()
	{
		//Check if the request is ajax request
		if($this->input->is_ajax_request()) 
		{
			//because we use ajax with method post, so we need to use function post in input class.
			//get id of the current post.
			$data['projectID']        = (int) $this->input->post('projectID'); 
			$data['studentID']    = htmlspecialchars($this->session->userdata('UserID'));
			$data['content']        = $this->input->post('comment');
			$data['created_time']   = time();
			$data['likes'] =(int) $this->input->post('likes');
				
			//projectID cannot be null
			if($data['projectID'] == '')
			{
				echo json_encode(array('err' => 1, 'content' => 'Oops, The post is not available yet!'));
				//stop the action
				die; 
			}
	
			//comment content cannot be null
			if($data['content'] == '')
			{
				echo json_encode(array('err' => 1, 'content' => '* fields cannot be empty!'));
				die;
			}
	
			//insert the comment
			$this->load->model('project_model', 'post');
			if($this->post->insert('comment_for_project', $data))
			{
				//create new comment element to insert to template
				$author = '<strong>'.$data['studentID'].'</strong>';
	
				$new_comment = '
				<li class="row comment-item" id="comment-'.$data['projectID'].'">
        				<img src="http://www.gravatar.com/avatar/?d=mm" alt="" class="img-circle pull-left">
        				<div class="comment-content">
        					<div class="comment-author rows">
        						'.$author.'
        						<span class="comment-time pull-right">'.date('F j, Y - g:i a', $data['created_time']).'</span>
        					</div>
        					<span>'.$data['content'].'</span>
        					<button class="comment-vote pull-right">Like</button>
        					<span class="count pull-right">'.$data['likes'].'</span>
        				</div>
        		</li>';
				//display thank you message if successfully submit comment
				echo json_encode(array('err' => 0, 'new_comment' => $new_comment));
				die;
			}
			//else display try again message if cannot submit
			else
			{
				echo json_encode(array('err' => 1, 'content' => 'Oops! We cannot get your comment at this time, pls try again later!'));
				die;
			}
		}
	}
}

?>
