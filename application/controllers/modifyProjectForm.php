<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* @author Andrew
* controller class for handling modify project detail page
*/
class ModifyProjectForm extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('project_model');
		}
		else
		{
			redirect('login');
		}
	}
	
	function index($id)
	{	
		//get the equipment list from model
		$this->load->model('project_model');
		$this->load->model('equipment_model');
			
        // Load templates
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
        
		$equipmentlist = $this->project_model->get_equipment_project($id);	
		$equipmentids = array();
		foreach ($equipmentlist as $rowb){
			array_push ($equipmentids, $rowb->equipmentID);
		}
		$data["equipmentList"] =  $equipmentids;	
		$data["tableContent"] = $this->equipment_model->get_equipment_list();
		$data['query'] = $this->project_model->select_project_by_projectID($id);
		
		$this->load->view('project/modifyProjectForm', $data);
		$this->load->view('templates/footer');
		
	}
	
	/**
	 * @author Andrew
	 * Insert into database
	 */
	function update_to_db()
	{
			
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_error_delimiters('<div class="errorMsg">', '</div>');
		
		$this->form_validation->set_rules('projectName', 'Project Name', 'trim|required|max_length[50]|xss_clean');
		$this->form_validation->set_rules('contentarea', 'Description', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			//get the equipment list from model
			$this->load->model('project_model');
			$this->load->model('equipment_model');
			
			
	        // Load templates
			$this->load->view('templates/header');
			$this->load->view('templates/headernav');
	        
			$id= $_POST['projectID'];
			$equipmentlist = $this->project_model->get_equipment_project($id);	
			$equipmentids = array();
			foreach ($equipmentlist as $rowb){
				array_push ($equipmentids, $rowb->equipmentID);
			}
			$data["equipmentList"] =  $equipmentids;	
			$data["tableContent"] = $this->equipment_model->get_equipment_list();
			$data['query'] = $this->project_model->select_project_by_projectID($id);
			
			$this->load->view('project/modifyProjectForm.php', $data);
		}
		else
		{
			// Load the project-model
			$this->load->model('project_model');
			$this->project_model->update_into_db();
			//update the project equipment relationship
			$projectID = $_POST['projectID'];
			if ($_POST['equipmentListResult'] != null || $_POST['equipmentListResult'] != ""){
				$equipmentList = explode ( "," ,$_POST['equipmentListResult']);
				$this->project_model->update_equipment_project_relation($equipmentList,$projectID);
			}
			redirect('modifyProject');
		}
	}
}
		