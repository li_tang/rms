<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of project
 *
 * @author Farida
 */
class Project extends CI_Controller {
	
    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('project_model');
		}
		else
		{
			redirect('login');
		}
		
		$this->load->library("table");
		$this->load->model("project_model");
		$this->load->library("pagination");         
	}
    
    function index(){
        $this->load->library("table");
        $this->load->model("project_model");
        $this->load->library("pagination");
        
        $config['base_url'] = base_url().'project/index';
        $config['total_rows'] = $this->db->get('project')->num_rows();
        $config['per_page'] = 5;
        $this->pagination->initialize($config);
        
        $startingPageIndex = ($this->uri->segment(3) - 1)* $config['per_page'];
        if($startingPageIndex<0)
        {
                $startingPageIndex = 0;
        }
        
        //$data["tableContent"] = $this->_processList($this->project_model->get_all_projects($config['per_page'],$startingPageIndex));
        $data["tableContent"] = $this->project_model->get_all_projects($config['per_page'],$startingPageIndex);
        
		// Load templates
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
        // loading form view
		$this->load->view('project/project',$data);
		$this->load->view('templates/footer');
    }
    
    /**
     * search project
     */
    function search(){
    	$config['base_url'] = base_url().'project/search';
    	$config['per_page'] = 5;
    	
    	$searchStr = $this->input->get('searchProject', TRUE);
    	$searchOpt = $this->input->get('searchOpt', TRUE);
    	
    	$config['suffix'] = "?searchProject=$searchStr&searchOpt=$searchOpt";
    	$config['first_url'] = $config['base_url'].$config['suffix'];
    	
    	if($searchOpt=='projectName')
    	{
    		$searchOpt = 'title';
    	}
    	else if ($searchOpt =='course')
    	{
    		$searchOpt = 'coursecode';
    	} 
    	
    	
    	$startingPageIndex = ($this->uri->segment(3) - 1)* $config['per_page'];
    	if($startingPageIndex<0)
    	{
    		$startingPageIndex = 0;
    	}
    	$pageSize = $config['per_page'];
    	if($searchStr==''||$searchOpt=='')
    	{
    		$query = $this->project_model->get_all_projects($config['per_page'],$startingPageIndex);
    	}
    	else
    	{
    		$query = $this->project_model->search_project($searchStr,$searchOpt,$pageSize,$startingPageIndex);
    	}
    	$config['total_rows'] = $this->project_model->count_search_project($searchStr,$searchOpt);
    	$this->pagination->initialize($config);
    	$data["tableContent"] = $query;

    	
    	//rendering view
    	$this->load->view('templates/header');
    	$this->load->view('templates/headernav');
    	$this->load->view('project/project',$data);
    	$this->load->view('templates/footer');
    }
}

?>
