<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* @author Andrew
* controller class for handling add equipment detail page
*/
class AddEquipment extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
                if($this->session->userdata('status')=='isLoggedIn')
                {
                    $this->load->model('equipment_model');
                }
                else
                {
                    redirect('login');
                }
	}
	
	function index()
	{	
        // Load templates
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
        // loading form view
		$this->load->view('equipment/addEquipment.php');
		$this->load->view('templates/footer');
	}
	/**
	 * @author Andrew
	 * Insert into database
	 */
	function insert_to_db()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_error_delimiters('<div class="errorMsg">', '</div>');
		
		$this->form_validation->set_rules('equipmentName', 'Equipment Name', 'trim|required|max_length[50]|xss_clean');
		$this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
		$this->form_validation->set_rules('manufacturer', 'Manufacturer', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
				// Load templates
			$this->load->view('templates/header');
			$this->load->view('templates/headernav');
	        // loading form view
			$this->load->view('equipment/addEquipment.php');
			$this->load->view('templates/footer');
		}
		else
		{
        	// Load the equipment-model
			$this->load->model('equipment_model');
			$this->equipment_model->insert_into_db();
			// Call the listEquipmentStaff controller
	        redirect('listEquipment');
		}
	}
	
}

