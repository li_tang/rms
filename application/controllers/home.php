<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @author Wang Qi
 * Home controller for display the login home page
 */
class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // Loading the announcement_model
         
        if($this->session->userdata('status')=='isLoggedIn')
        {
            $this->load->model('announcement_model');
        }
        else
        {
            redirect('login');
        }
    }
        
    
    function index()
    {
        // Load the header and headernav for the page  
        $this->load->view('templates/header');
        $this->load->view('templates/headernav');
       
        // Based on the different category of the user to redirect the user to different pages
        if ($this->session->userdata('Category')=='Student')
        {      
            /**
             * Get announcements to display on the student home page
             * Call the announcement_model to retrive announcement from the database
            */
            $data['announcement'] = $this->announcement_model->get_announcement();    
            // Load the student page and the footer
            $this->load->view('index',$data);
            $this->load->view('templates/footer');
        }
        else if ($this->session->userdata('Category')=='Staff')
        {
            // Call the announcement_model to retrive announcement from the database to display on the staff home page
            $data['announcement'] = $this->announcement_model->get_announcement();
            $this->load->view('announcement/index',$data);
            $this->load->view('templates/footer');
        }
    }
}
