<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @author LIU XINLU
 * This controller contains insert order method, which gets value from makeorder view page 
 * This controller contains email method, which will send user a confirmation email after he made bookings.
 */
class order extends CI_Controller 
{
    
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('order_model');
		}
		else
		{
			redirect('login');
		}
	}

    function index($id)
	{
		$this->load->model('equipment_model');
		$equipmentArr = $this->equipment_model->get_equipment_info($id);
		$data['equipment'] = $this->equipment_model->get_equipment_info($id);
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('bookings/makeOrder',$data);
		$this->load->view('templates/footer');
	}
     
    /**
     * get equipment name by id
     */
    private function getEquipmentName($equipmentId)
    {
        $this->load->model('equipment_model');
        $equipment = $this->equipment_model->get_equipment_info($equipmentId);
        return $equipment[0]->equipmentName;
    }
    
    /**
     * caculate end date based on collection date and loan duration
     * @param  $date collection date
     * @param  $days loan duration 
     */
    function addDaysWithDate($date,$days){
        $date = strtotime("+".$days." days", strtotime($date));
        return  date("Y-m-d", $date);
    }
    
    /**
     * update quantity of equipment after insertion
     */
    function update_quantity()
    {
        $this->load->model('equipment_model');
        $id = intval($this->input->post('eid'));
        $quantitySum=intval($this->input->post('totalquantity'))-intval($this->input->post('quantity'));
        $this->equipment_model->update_quantity($quantitySum,$id);
    }
    
    
    function insert()
    {
        $loanPeriod = intval($this->input->post("loanPeriod"));
        //echo $loanPeriod;
        
        $this->load->model('order_model');
        
        $email= $this->session->userdata('email');
        $mobile= $this->session->userdata('mobileNo');
        $startDate=$this->input->post('startDate');
        $CollDate = str_replace('/', '-', $startDate);
        //echo date('Y-m-d',strtotime($CollDate));
        $endDate= $this->addDaysWithDate($CollDate, $loanPeriod);
        //echo $endDate;
        $quantity=$this->input->post('quantity');
        $id = intval($this->input->post('eid'));
        
        $orderStatus = "Pending";
        $studentID=  $this->session->userdata('UserID');
        $equipmentID = intval($id);
        //echo $equipmentID;
        $arr = array('email'=>$email,'mobileNo'=>$mobile,'startDate'=>date('Y-m-d', strtotime($CollDate))
            ,'endDate'=>$endDate,'quantity'=>$quantity, 'orderStatus'=>$orderStatus, 'studentID'=>$studentID,'equipmentID'=>$equipmentID);
        
        $orderid = $this->order_model->order_insert($arr);
        $this->update_quantity();
        $this->Email($arr,$orderid);
    }

    /**
     * email function
     * @param unknown $data view data
     * @param unknown $orderid orderid 
     */
    function Email($data,$orderid)
    {
        $email=$this->session->userdata('email');
        
        $config = Array(
            'protocol'=>'smtp',
            'smtp_host'=>'ssl://smtp.googlemail.com',
            'smtp_port'=>465,
            'smtp_user'=>'uqresourcemanagement@gmail.com',
            'smtp_pass'=>'43112580',
            'mailtype'=> 'html', 
            'charset' => 'iso-8859-1'
        );
        
        $this->load->library('email',$config);
        
        $this->email->set_newline("\r\n");
        
        $this->email->from('uqresourcemanagement@gmail.com','UQ admin');
        $this->email->to($email);
        $this->email->subject('Equipment Booking Confirmation');
        $msg = "Dear student, you have booked ".$data['quantity']." piece of equipment '".$this->getEquipmentName($data['equipmentID'])."', please collect on ".$data['startDate'].".Your booking ID is ".$orderid.".";
        $this->email->message($msg);
        
        if($this->email->send())
        {
           $this->load->view('templates/header');
           $this->load->view('templates/headernav');
           $this->load->view('bookings/congrats.php');
            //echo 'Your email sent sucessfully!';
           $this->load->view('templates/footer');
            
        }
        else
        {
            show_error($this->email->print_debugger());
        }
    }
    


}