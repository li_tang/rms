<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @author li.tang
 * controller class for handling all the listing and searching
 */
class ListEquipment extends CI_Controller {
	private $startingPageIndex;
	var $paginationConfig = array();
	
	public function __construct()
	{
		parent::__construct();
   
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('equipment_model');
		}
		else
		{
			redirect('login');
		}
	
		// Your own constructor code
		$this->load->library("table");
		$this->load->model("equipment_model");
		$this->load->library("pagination");
	}

	/**
	 * @author li.tang
	 * This function displays all the records for equipment which is the default function
	 * for this controller
	 */
	public function index()
	{
		$this->paginationConfig['base_url'] = base_url().'listEquipment/index';
		$this->paginationConfig['total_rows'] = $this->db->get('equipment')->num_rows();
		$this->paginationConfig['per_page'] = 10;
		$this->pagination->initialize($this->paginationConfig);
		
		$this->startingPageIndex = ($this->uri->segment(3) - 1)* $this->paginationConfig['per_page'];
		if($this->startingPageIndex<0)
		{
			$this->startingPageIndex = 0;
		}
		//checking whether page exist
		if ( ! file_exists('application/views/equipment/listEquipment.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		//populate different view data based on login role
		if($this->session->userdata('Category')=='Student')
		{
			$data["tableContent"] = $this->equipment_model->get_all_equipment($this->paginationConfig['per_page'],$this->startingPageIndex);
		}
		else if($this->session->userdata('Category')=='Staff')
		{
			$data["tableContent"] = $this->_processListStaff($this->equipment_model->get_all_equipment($this->paginationConfig['per_page'],$this->startingPageIndex));
		}
		$data["found"] = true;
		//rendering view
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('equipment/listEquipment',$data);
		$this->load->view('templates/footer');
	}
	
	/**
	 * @author li.tang
	 * @param search string entered by user
	 * This function will do a fuzzy search based on project, course, name of equipment
	 */
	public function search()
	{
		$this->paginationConfig['base_url'] = base_url().'listEquipment/search';
		$this->paginationConfig['per_page'] = 10;
		$this->startingPageIndex = ($this->uri->segment(3) - 1)* $this->paginationConfig['per_page'];
		if($this->startingPageIndex<0)
		{
			$this->startingPageIndex = 0;
		}
		
		$searchStr = $this->input->get('searchEquipment', TRUE);
		$searchOpt = $this->input->get('searchOpt', TRUE);
		
		$this->paginationConfig['suffix'] = "?searchEquipment=$searchStr&searchOpt=$searchOpt";
		$this->paginationConfig['first_url'] = $this->paginationConfig['base_url'].$this->paginationConfig['suffix'];
		
		if($searchOpt=='projectName')
		{
			$searchOpt = 'title';
		}
		elseif ($searchOpt =='course')
		{
			$searchOpt = 'coursecode';
		}
		
		$count = 0;
		$pageSize = $this->paginationConfig['per_page'];
		if($searchStr==''||$searchOpt=='')
		{
			$query = $this->equipment_model->get_all_equipment($this->paginationConfig['per_page'],$this->startingPageIndex);
			$count = $this->db->get('equipment')->num_rows();
		}
		else
		{
			$query = $this->equipment_model->search_equipment($searchStr,$searchOpt,$pageSize,$this->startingPageIndex);
			$count = $this->equipment_model->count_search_equipment($searchStr,$searchOpt);
		}
		$data["found"] = true;
		//populate different view data based on login role
		if($count == 0)
		{
			$data["found"] = false;
		}
		else if($this->session->userdata('Category')=='Student')
		{
			$data["tableContent"] = $query;
		}
		else if($this->session->userdata('Category')=='Staff')
		{
			$data["tableContent"] = $this->_processListStaff($query);
		}
		
		$this->paginationConfig['total_rows'] = $count;
		$this->pagination->initialize($this->paginationConfig);
		//rendering view
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
		$this->load->view('equipment/listEquipment',$data);
		$this->load->view('templates/footer');
	}
	
	/**
	 * @author andrew
	 * @param search string entered by user
	 * This function process the query result and return a desired format
	 */
	
	private function _processListStaff($queryResult)
	{
		$tmpl = array (
				'table_open'          => '<table border="1" cellpadding="2" cellspacing="1" class="regular">',
		
				'heading_row_start'   => '<tr>',
				'heading_row_end'     => '</tr>',
				'heading_cell_start'  => '<th>',
				'heading_cell_end'    => '</th>',
		
				'row_start'           => '<tr>',
				'row_end'             => '</tr>',
				'cell_start'          => '<td>',
				'cell_end'            => '</td>',
		
				'row_alt_start'       => '<tr>',
				'row_alt_end'         => '</tr>',
				'cell_alt_start'      => '<td>',
				'cell_alt_end'        => '</td>',
		
				'table_close'         => '</table>'
		);
		
		$this->table->set_template($tmpl);
		$this->table->set_heading('','Equipment Name','Quantity','Description','Location',"","");
		foreach($queryResult as $row)
		{
			
			if ($row->filecontent == null){
				$image = "<img src='".base_url()."images/noimage.gif' alt='No Image' width = 65 height = 65>";
			} else {
				$image = '<img src="data:'.$row->filetype.';base64,' . base64_encode( $row->filecontent ) . '" width = 65 height = 65/>';
			}	
			$modify = anchor('modifyEquipment/index/'.$row->equipmentID,'Modify');
			if($this->equipment_model->equipment_is_ordered($row->equipmentID))
			{
				$delete = "On loan";
			}
			else
			{
 				//$delete = anchor('deleteEquipment/index/'.$row->equipmentID,'Delete', array('onClick' => "return confirm('Are you sure you want to delete?')"));
 				$delete = anchor('deleteEquipment/index/'.$row->equipmentID,'Delete');
			}
			$this->table->add_row(array($image,$row->equipmentName, $row->quantity,$row->description,$row->location,$modify,$delete));
		}
	}
	
}