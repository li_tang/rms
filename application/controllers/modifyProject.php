<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* @author Andrew
* controller class for handling modify project detail page
*/
class ModifyProject extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status')=='isLoggedIn')
		{
			$this->load->model('project_model');
		}
		else
		{
			redirect('login');
		}
	}
	
	function index()
	{	
		//get the equipment list from model
		$this->load->model('equipment_model');
		
        // Load templates
		$this->load->view('templates/header');
		$this->load->view('templates/headernav');
        
		$data['query'] = $this->project_model->select_project_by_userID();
		
		$this->load->view('project/modifyProject.php', $data);		
		$this->load->view('templates/footer');
	}
	

}

