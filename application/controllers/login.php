<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @author WANG QI
 * This controller controls the login/logout of the user
 * Provide Authentication by using session libaray
 * Give authorization to user to view different pages based on their category
 */
class Login extends CI_Controller {

	public function index()
	{
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
		// Load the header,footer and view the login page
		$this->form_validation->set_rules('userID', 'UserID','trim|callback_checkUserID|xss_clean');
		$this->form_validation->set_rules('password', 'Password','trim|callback_checkPassword|xss_clean');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('login');
			$this->load->view('templates/footer');
		}
	}
	/**
	 * check if it is a valid userID
	 * @return true if it is
	 */
	public function checkUserID()
	{
		$this->load->model('login_model');
		// Get the user input as 'UserID'and pass to the user_select() in login_model
		// To check whether the UserID is valid
		$user=$this->login_model->user_select($_POST['UserID']);
		if($user)
		{
			return True;
		}
		else 
		{ 
			$this->form_validation->set_message('checkUserID', 'Please enter a correct UserID');
			return FALSE;    
		}	
	}
	
	/**
	 * This function provide the authentication for user login
	 * @return true if user is authenticated
	 */
	public function checkPassword()
	{
		// Load the model
		$this->load->model('login_model');
		$user=$this->login_model->user_select($_POST['UserID']);
		if($user)
		{
			// To check whether the password input by the user match the password stored in the database
			// Provide Authentication
			if($user[0]->Password==MD5($_POST['Password']))
			{
				// Once the user login sucessfully, create a session to store the user's information
				$this->load->library('session');
				$arr=array('UserID'=>$user[0]->UserID,'Category'=>$user[0]->Category,'Name'=>$user[0]->Name,
				'email'=>$user[0]->email,'mobileNo'=>$user[0]->mobileNo,
				'course1'=>$user[0]->course1,'course2'=>$user[0]->course2,'course3'=>$user[0]->course3,'course4'=>$user[0]->course4,'status'=>
					'isLoggedIn');
				$this->session->set_userdata($arr);
				// Call the checksession() method 
				$this->checksession();
			}
			else
			{
				// If the password don't match, redirect the user to the login page
				$this->form_validation->set_message('checkPassword', 'Please enter the correct password');
				return FALSE;
			} 
		}
		else
		{
			return true;
		}
	}
	
	/**
	* This function provides the authorization 
	*/
	public function checksession()
	{
		// Check the user's category, redirect the user to different pages   
		if($this->session->userdata('Category')=='Student')
		{   
			// Call the home controller
			redirect('home');
		}
		else if ($this->session->userdata('Category')=='Staff')
		{
		   // Call the home controller
			redirect('home');
		}
		else {
			// If session don't exist, redirect the user to the login page
			redirect('');
		}
	}
	
	 /**
	 * This function provide the logout 
	 */
	public function logout()
	{
		// Unset the session data and destroy all the session information
		$this->session->unset_userdata('UserID');
		$this->session->sess_destroy();
		redirect('');
	}
}
?>
