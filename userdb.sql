-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2013 at 12:23 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `userdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `userdb`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `userdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--
DROP TABLE IF EXISTS `user`;

CREATE TABLE IF NOT EXISTS `user` (
  `UserID` varchar(8) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Password` text NOT NULL,
  `Category` varchar(10) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobileNo` varchar(45) DEFAULT NULL,
  `course1` varchar(45) DEFAULT NULL,
  `course2` varchar(45) DEFAULT NULL,
  `course3` varchar(45) DEFAULT NULL,
  `course4` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Name`),
  UNIQUE KEY `UserID` (`UserID`),
  UNIQUE KEY `UserID_2` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserID`, `Name`, `Password`, `Category`, `email`, `mobileNo`, `course1`, `course2`, `course3`, `course4`) VALUES
('s4222222', 'James Wheeler', 'fcea920f7412b5da7be0cf42b8c93759', 'Student', 'james_wheeler01@hotmail.com', '0487654321', 'DECO3800', 'INFS3500', 'DECO2800', 'COMP3200'),
('t234', 'Jim Steel', 'fcea920f7412b5da7be0cf42b8c93759', 'Staff', 'j_steel@gmail.com', '0498761234', NULL, NULL, NULL, NULL),
('s123', 'LIU XIN LU', 'fcea920f7412b5da7be0cf42b8c93759', 'Student', 'xinlu0628@hotmail.com', '0412345678', 'DECO3801', 'DECO4204', 'DECO2800', 'DECO3200'),
('s234', 'Sarah Cronk', 'fcea920f7412b5da7be0cf42b8c93759', 'Student', 'sarah_green@hotmail.com', '0491234567', 'INDS2300', 'INFS3500', 'DECO2800', 'DECO3600'),
('t123', 'Steven Viller', 'fcea920f7412b5da7be0cf42b8c93759', 'Staff', 'steven_viller@uq.edu.au', '0443123456', NULL, NULL, NULL, NULL),
('s4111111', 'Tom Smith', 'fcea920f7412b5da7be0cf42b8c93759', 'Student', 'Tom_Smith@gmail.com', '0498765432', 'INFS2500', 'COMP3204', 'DECO4300', 'INFS4300');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
