-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: rms
-- ------------------------------------------------------
-- Server version	5.5.25a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `rms`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `rms` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rms`;

--
-- Table structure for table `announcement`
--

DROP TABLE IF EXISTS `announcement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement` (
  `annID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `content` varchar(4500) DEFAULT NULL,
  `staffID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`annID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commentforequipment`
--

DROP TABLE IF EXISTS `comment_for_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_for_equipment` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `created_time` int(11) DEFAULT NULL,
  `content` varchar(4500) DEFAULT NULL,
  `studentID` varchar(50) DEFAULT NULL,
  `equipmentID` int(11) DEFAULT NULL,
  `likes` int(11) DEFAULT NULL,
  PRIMARY KEY (`commentID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commentforproject`
--

DROP TABLE IF EXISTS `comment_for_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_for_project` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `created_time` int(11) DEFAULT NULL,
  `content` varchar(45) DEFAULT NULL,
  `projectID` int(11) DEFAULT NULL,
  `studentID` varchar(50) DEFAULT NULL,
  `likes` int(11) DEFAULT NULL,
  PRIMARY KEY (`commentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment` (
  `equipmentID` int(11) NOT NULL AUTO_INCREMENT,
  `equipmentName` varchar(45) DEFAULT NULL,
  `filename` varchar(450) DEFAULT NULL,
  `filetype` varchar(45) DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `filecontent` mediumblob,
  `quantity` int(11) DEFAULT NULL,
  `loanStatus` varchar(45) DEFAULT NULL,
  `loanPeriod` int(11) DEFAULT NULL,
  `description` varchar(4500) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `manufacturer` varchar(45) DEFAULT NULL,
  `staffID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`equipmentID`),
  KEY `staffID` (`staffID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `orderID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(450) DEFAULT NULL,
  `mobileNo` varchar(45) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `orderStatus` varchar(45) DEFAULT NULL,
  `studentID` varchar(50) DEFAULT NULL,
  `equipmentID` int(11) DEFAULT NULL,
  PRIMARY KEY (`orderID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `projectID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `coursecode` varchar(45) NOT NULL,
  `description` varchar(4500) DEFAULT NULL,
  `filename` varchar(45) DEFAULT NULL,
  `filetype` varchar(45) DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `filecontent` mediumblob,
  `likeNum` int(11) DEFAULT NULL,
  `studentID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`projectID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `eqpt_usage_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eqpt_usage_history` (
  `recordID` int(11) NOT NULL AUTO_INCREMENT,
  `projectID` int(11) NOT NULL,
  `equipmentID` int(11) NOT NULL,
  PRIMARY KEY (`recordID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment_for_equipment`
--
ALTER TABLE `comment_for_equipment`
  ADD CONSTRAINT `fk_comment_equipment` FOREIGN KEY (`equipmentID`) REFERENCES `equipment` (`equipmentID`) ON DELETE CASCADE;

--
-- Constraints for table `comment_for_project`
--
ALTER TABLE `comment_for_project`
  ADD CONSTRAINT `fk_comment_project` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`) ON DELETE CASCADE;

--
-- Constraints for table `eqpt_usage_history`
--
ALTER TABLE `eqpt_usage_history`
  ADD CONSTRAINT `fk_equipment_history` FOREIGN KEY (`equipmentID`) REFERENCES `equipment` (`equipmentID`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_project_history` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`) ON DELETE CASCADE;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_equipment` FOREIGN KEY (`equipmentID`) REFERENCES `equipment` (`equipmentID`) ON DELETE CASCADE;
