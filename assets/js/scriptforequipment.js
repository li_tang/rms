/**
* @author yanbo.zhao
* function to submit comment by ajax without refreshing the page
*/
$(function() {
	$('.submit-comment').click(function(){
		var elem = $(this);
		//use ckeditor to replace textarea and update content inside
		for ( instance in CKEDITOR.instances ){
		     CKEDITOR.instances[instance].updateElement();
		     CKEDITOR.instances[instance].setData('');
		}
		//getting form data to send it.
		var data = $('#comment_form').serialize();
		$.ajax({
			beforeSend: function() {
				//disable the button click and show the loading image
				elem.attr('disabled', 'disabled').addClass('disabled');
				$('.notice').html('<span class="loading"></span>');
			},
			//corresponding to function ajax_submit in viewEquipment controller
			url: 'viewEquipment/ajax_submit',
			type: 'post',
			dataType: 'json',
			data: data,
			success: function(data) {
				//reactive button click
				elem.removeAttr('disabled').removeClass('disabled');
				if(data.err == 1)
				{
					//display notice
					$('.notice').html(data.content);
				}
				else
				{
					$().val('');
					//reset input value of comment form
					$('#comment_form, #comment_form textarea').val('');
					$('.notice').html(data.content);
					 //Insert new comment to the top of comments list
					$('.comment-list ul').prepend(data.new_comment);

					//scroll the document to the new comment.
					$('html, body').animate({
					    scrollTop: $(".comment-list").offset().top
					}, 800);
				}
			}
		});
		return false;
	})
});